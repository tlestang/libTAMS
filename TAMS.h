#ifndef _TAMS
#define _TAMS

#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <cstdlib>
#include <vector>
#include <climits>
#include <cfloat>

template<typename T>
class TAMS{
 public:
  TAMS(int Nc, int Nt);
  TAMS(int Nc, int Nt, int ndof, int writingPeriod);
  ~TAMS();
  void setActiveTraj(int j);
  bool openStateFile();
  void setCostFunction(double Cf, int t);
  void setCostFunction(double Cf, int t, T* ptToState);
  void setState(T ste, int t);
  void setTBranch(int tBranch);
  void setParent(int parentIdx);
  bool writeState(T* ptToste, int t);
  void writeCostFunction(std::string directoryName, int iter);
  void writeCostFunction(std::string directoryName, int iter, int k);
  void readCostFunction(std::string directoryName, int iter);
  void writeTrajectories(std::string directoryName, int iter);
  void readTrajectories(std::string directoryName, int iter);
  void writeTimeStamps(std::string directoryName, int iter);
  void writeTimeStamps(std::string directoryName, int iter, int k);
  void readTimeStamps(std::string directoryName, int iter);
  void drawParent();
  void computeZ();
  virtual void computeMaxima();
  virtual double updateMaxima();
  double getMax();
  double getMax(int j);
  T getRestart();
  int getRestart(T* ptToste);
  int getRestartTime();
  T* getRestartState();
  int copyTrajToBranchingPoint();
  int makeSelection();
  double getParentTraj();
  double getActiveTraj();
  double getZ();
  bool is_extinct();
 protected:
  int m_Nc;
  int m_Nt;
  int m_jj; /*Index of active trajectory*/
  int m_tBranch;
  int m_tRestart;
  int m_parent;
  int *m_idx; /*Array of indexes. First m_K members are the indices of active trajectories*/
  int m_K;
  int m_ndof; /*Nb of degress of freedom for the dynamics. Useful if states are written on disk along the computation of the dynamics*/
  int m_writingPeriod;
  int m_lastWritingTime;
  double *m_amax; /*Array containing the maxima over each running trajectory*/
  double m_Z; /*Current threshold*/
  double **m_costFunction;
  T **m_stateArray; /*2D array containig the state of every trajectory, at each timestep. Used for low dimensional systems for which this can fit on memory.*/
  T *m_state; /*Buffer for a state of the system. Used for high dimensional systems. Useful to read/write the pointed state on disk*/
  std::vector<std::vector<int>> m_timeStamps; /*Vector containing the times at which states have been written on disk*/
  std::ofstream m_stateFile;
};
template<typename T>
TAMS<T>::TAMS(int Nc, int Nt) : m_Nc(Nc), m_Nt(Nt)
{
  /*Minimal constructor. Allocates arrays. and initializes m_idx*/
  m_costFunction = new double*[Nc];
  m_stateArray = new T*[Nc];
  m_state = NULL;
  m_idx = new int[Nc];
  m_amax = new double[Nc];
  for(int j=0;j<Nc;j++)
    {
      m_costFunction[j] = new double[Nt];
      m_stateArray[j] = new T[Nt];
      /*Initializes m_idx*/
      m_idx[j] = j;
      m_amax[j] = -DBL_MAX;
     }
}
template<typename T>
TAMS<T>::TAMS(int Nc, int Nt, int ndof, int writingPeriod) : m_Nc(Nc), m_Nt(Nt), m_ndof(ndof), m_writingPeriod(writingPeriod)
{
  /*This version of the constructor is to be used when the dynamics have so much degrees of freedom 
that it is impossible to keep track of the states in memory. In this case, states are written/read 
from disk*/
  m_costFunction = new double*[Nc];
  m_state = new T[ndof];
  m_stateArray = NULL;
  m_idx = new int[Nc];
  m_amax = new double[Nc];
  for(int j=0;j<Nc;j++)
    {
      m_costFunction[j] = new double[Nt];
      m_idx[j] = j;
      m_timeStamps.push_back(std::vector<int>(0)); //Adds a new row to the timeStamps array
      m_amax[j] = -DBL_MAX;
    }
  m_lastWritingTime = -m_writingPeriod-1;
}
template<typename T>
TAMS<T>::~TAMS()
{
  if(m_stateArray != NULL)
    {
      for (int j=0;j<m_Nc;j++){delete[] m_stateArray[j];};
      delete[] m_stateArray;
    }
  if(m_state != NULL)
    {
      delete[] m_state;
    }
  
  for (int j=0;j<m_Nc;j++)
    {
      delete[] m_costFunction[j];
    }
  delete[] m_costFunction;
  delete[] m_amax;
  delete[] m_idx;
}
template<typename T>
void TAMS<T>::setState(T ste, int t)
{
  m_stateArray[m_jj][t] = ste;
}
template<typename T>
void TAMS<T>::setCostFunction(double Cf, int t)
{
  m_costFunction[m_jj][t] = Cf;
  if(Cf>m_amax[m_jj])
    {
      m_amax[m_jj] = Cf;
    }
}
template<typename T>
void TAMS<T>::setCostFunction(double Cf, int t, T* ptToState)
{
  m_costFunction[m_jj][t] = Cf;
  if(Cf>m_amax[m_jj])
    {
      m_amax[m_jj] = Cf;
      if(t-m_lastWritingTime > m_writingPeriod)
	{
	  writeState(ptToState, t);
	}
    }
}
template<typename T>
void TAMS<T>::setActiveTraj(int j)
{
  m_jj = m_idx[j];
  m_lastWritingTime = -m_writingPeriod-1;
}
template<typename T>
bool TAMS<T>::openStateFile()
{
  std::stringstream dummyss;
  dummyss << "traj_"<<m_jj<<".ste";
  if(m_stateFile.is_open())
    {
      m_stateFile.close();
    }
  m_stateFile.open(dummyss.str().c_str(), std::ios::binary);
  dummyss.str(std::string());
  dummyss.clear();
  m_timeStamps[m_jj].clear();
  
  return m_stateFile.is_open();
}
template<typename T>
bool TAMS<T>::writeState(T* ptToState, int t)
{
  m_timeStamps[m_jj].push_back(t);
  if(m_stateFile.is_open())
    {
      m_stateFile.write((char*)ptToState, m_ndof*sizeof(T));
      m_lastWritingTime = t;
      return false;
    }
  else
    {
      std::cout << "Error in TAMS<T>::writeState : stateFile is closed" << std::endl;
      return true;
    }
}
template<typename T>
void TAMS<T>::writeTimeStamps(std::string directoryName, int iter)
{
  std::ofstream trajOutputFile;
  std::stringstream fileName;
  std::string fullFileName;
  for(int j=0;j<m_Nc;j++)
    {
      fileName << "traj_" << j << "_timestamps_iter_" << iter << ".dat";
      fullFileName = directoryName + fileName.str();

      trajOutputFile.open(fullFileName.c_str(), std::ios::binary);
      trajOutputFile.write((char*)&m_timeStamps[j][0], m_timeStamps[j].size()*sizeof(int));
      trajOutputFile.close();
      fileName.str(std::string());
      fileName.clear();
    }
}
template<typename T>
void TAMS<T>::writeTimeStamps(std::string directoryName, int iter, int k)
{
  std::ofstream trajOutputFile;
  std::stringstream fileName;
  std::string fullFileName;
  int j = m_idx[k];
  fileName << "traj_" << j << "_timestamps_iter_" << iter << ".dat";
  fullFileName = directoryName + fileName.str();
  trajOutputFile.open(fullFileName.c_str(), std::ios::binary);
  trajOutputFile.write((char*)&m_timeStamps[j][0], m_timeStamps[j].size()*sizeof(int));
  trajOutputFile.close();
  fileName.str(std::string());
  fileName.clear();
}
template<typename T>
void TAMS<T>::readTimeStamps(std::string directoryName, int iter)
{
  int tsps, sizeInBytes, length;
  std::ifstream trajOutputFile;
  std::stringstream fileName;
  std::string fullFileName;
  for(int j=0;j<m_Nc;j++)
    {
      fileName << "traj_" << j << "_timestamps_iter_" << iter << ".dat";
      fullFileName = directoryName + fileName.str();

      trajOutputFile.open(fullFileName.c_str(), std::ios::binary);
      trajOutputFile.seekg(0,trajOutputFile.end);
      sizeInBytes = trajOutputFile.tellg();
      length = sizeInBytes/4;
      trajOutputFile.seekg(0,trajOutputFile.beg);
      for(int i=0;i<length;i++)
	{
	  trajOutputFile.read((char*)&tsps, sizeof(int));
	  m_timeStamps[j].push_back(tsps);
	}
      trajOutputFile.close();
      fileName.str(std::string());
      fileName.clear();
    }
}
template<typename T>
void TAMS<T>::writeTrajectories(std::string directoryName, int iter)
{
  std::ofstream trajOutputFile;
  std::stringstream fileName;
  std::string fullFileName;
  for(int j=0;j<m_Nc;j++)
    {
      fileName << "traj_" << j << "_iter_" << iter << ".dat";
      fullFileName = directoryName + fileName.str();

      trajOutputFile.open(fullFileName.c_str(), std::ios::binary);
      trajOutputFile.write((char*)&m_stateArray[j][0], m_Nt*sizeof(T));
      trajOutputFile.close();
      fileName.str(std::string());
      fileName.clear();
    }
}
template<typename T>
void TAMS<T>::readTrajectories(std::string directoryName, int iter)
{
  std::ifstream trajOutputFile;
  std::stringstream fileName;
  std::string fullFileName;
  for(int j=0;j<m_Nc;j++)
    {
      fileName << "traj_" << j << "_iter_" << iter << ".dat";
      fullFileName = directoryName + fileName.str();

      trajOutputFile.open(fullFileName.c_str(), std::ios::binary);
      trajOutputFile.read((char*)&m_stateArray[j][0], m_Nt*sizeof(T));
      trajOutputFile.close();
      fileName.str(std::string());
      fileName.clear();
    }
}
template<typename T>
void TAMS<T>::writeCostFunction(std::string directoryName, int iter)
{
  std::ofstream trajOutputFile;
  std::stringstream fileName;
  std::string fullFileName;
  for(int j=0;j<m_Nc;j++)
    {
      fileName << "traj_" << j << "_costFunc_iter_" << iter << ".dat";
      fullFileName = directoryName + fileName.str();

      trajOutputFile.open(fullFileName.c_str(), std::ios::binary);
      trajOutputFile.write((char*)&m_costFunction[j][0], m_Nt*sizeof(double));
      trajOutputFile.close();
      fileName.str(std::string());
      fileName.clear();
    }
}
template<typename T>
void TAMS<T>::writeCostFunction(std::string directoryName, int iter, int k)
{
  std::ofstream trajOutputFile;
  std::stringstream fileName;
  std::string fullFileName;
  int j = m_idx[k];
  fileName << "traj_" << j << "_costFunc_iter_" << iter << ".dat";
  fullFileName = directoryName + fileName.str();
  
  trajOutputFile.open(fullFileName.c_str(), std::ios::binary);
  trajOutputFile.write((char*)&m_costFunction[j][0], m_Nt*sizeof(double));
  trajOutputFile.close();
  fileName.str(std::string());
  fileName.clear();
}
template<typename T>
void TAMS<T>::readCostFunction(std::string directoryName, int iter)
{
  std::ifstream trajOutputFile;
  std::stringstream fileName;
  std::string fullFileName;
  for(int j=0;j<m_Nc;j++)
    {
      fileName << "traj_" << j << "_costFunc_iter_" << iter << ".dat";
      fullFileName = directoryName + fileName.str();

      trajOutputFile.open(fullFileName.c_str(), std::ios::binary);
      trajOutputFile.read((char*)&m_costFunction[j][0], m_Nt*sizeof(double));
      trajOutputFile.close();
      fileName.str(std::string());
      fileName.clear();
    }
}
template<typename T>
void TAMS<T>::computeZ()
{
  /*Update threshold*/
  m_Z = m_amax[0];
  for(int j=1;j<m_Nc;j++)
    {
      if(m_amax[j]<m_Z){m_Z = m_amax[j];}
    }
}
template<typename T>
void TAMS<T>::computeMaxima()
{
  double amax;
  for (int j=0;j<m_Nc;j++)
    {
      amax = m_costFunction[j][0];
      for(int t=1;t<m_Nt;t++)
	{
	  if(m_costFunction[j][t]>amax){amax = m_costFunction[j][t];}
	}
      m_amax[j] = amax;
    }
  computeZ();
}
template<typename T>
double TAMS<T>::updateMaxima()
{
  double amax;
  amax = m_costFunction[m_jj][0];
  for(int t=1;t<m_Nt;t++)
    {
      if(m_costFunction[m_jj][t]>amax){amax = m_costFunction[m_jj][t];}
    }
  m_amax[m_jj] = amax;
  computeZ();
  return amax;
}
template<typename T>
double TAMS<T>::getMax()
{
  return m_amax[m_jj];
}
template<typename T>
double TAMS<T>::getMax(int j)
{
  return m_amax[j];
}
template<typename T>
bool TAMS<T>::is_extinct()
{
  double maximumOfMaxima, extinctionRatio;
  maximumOfMaxima = m_amax[0];
  for(int j=1;j<m_Nc;j++)
    {
      if(m_amax[j]>maximumOfMaxima){maximumOfMaxima = m_amax[j];}
    }
  extinctionRatio = (maximumOfMaxima-m_Z)/maximumOfMaxima;
  // Returns TRUE if the relqtive difference between the greatest
  // max and the lowest is infoerior to 5 percent
  return (extinctionRatio<0.05);
}
template<typename T>
T TAMS<T>::getRestart()
{
  return m_stateArray[m_parent][m_tBranch];
}
template<typename T>
int TAMS<T>::getRestart(T* ptToste)
{
  /*For every iteration, for every traj, one must always have 
    timeStamps[j][0]<=tBranch
    This means the the first timeStamp of a given trajectory is *always* inferior or equal
    to the branching time
    By definition, the branching time is the *first* time at which the threshold is *strictly passed
    and the first timeStamps correspond to the time right next to it for deterministic systems, 
    or directly to the braching time for stochastic systems*/
  int count = 1;
  int tRestart, tnext;
  int nbrTimeStamps = m_timeStamps[m_parent].size();
  do
    {
      tnext = m_timeStamps[m_parent][count];
      count++;
    }while(tnext<=m_tBranch && count<nbrTimeStamps);
  /*If count==nbrTimeStamps-1 we reached the end of the timeStamps 
    vector and must exit the loop*/
  count = count - 2;
  m_tRestart = m_timeStamps[m_parent][count];
  std::stringstream dummyss;
  dummyss << "traj_"<<m_parent<<".ste";
  std::ifstream stateFile(dummyss.str().c_str(), std::ios::binary);
  stateFile.seekg(count*m_ndof*sizeof(T), std::ios::beg);
  stateFile.read((char*)ptToste, m_ndof*sizeof(T));
  stateFile.close();

  return m_tRestart;
}
template<typename T>
T* TAMS<T>::getRestartState()
{
  /*For every iteration, for every traj, one must always have 
    timeStamps[j][0]<=tBranch
    This means the the first timeStamp of a given trajectory is *always* inferior or equal
    to the branching time
    By definition, the branching time is the *first* time at which the threshold is *strictly passed
    and the first timeStamps correspond to the time right next to it for deterministic systems, 
    or directly to the braching time for stochastic systems*/
  int count = 1;
  int tRestart, tnext;
  int nbrTimeStamps = m_timeStamps[m_parent].size();
  do
    {
      tnext = m_timeStamps[m_parent][count];
      count++;
    }while(tnext<=m_tBranch && count<nbrTimeStamps);
  /*If count==nbrTimeStamps-1 we reached the end of the timeStamps 
    vector and must exit the loop*/
  count = count - 2;
  m_tRestart = m_timeStamps[m_parent][count];
  std::stringstream dummyss;
  dummyss << "traj_"<<m_parent<<".ste";
  std::ifstream stateFile(dummyss.str().c_str(), std::ios::binary);
  stateFile.seekg(count*m_ndof*sizeof(T), std::ios::beg);
  stateFile.read((char*)m_state, m_ndof*sizeof(T));
  stateFile.close();
    
  return m_state;
}
template<typename T>
int TAMS<T>::getRestartTime()
{
  return m_tRestart;
}
template<typename T>
double TAMS<T>::getZ()
{
  return m_Z;
}
template<typename T>
double TAMS<T>::getParentTraj()
{
  return m_parent;
}
template<typename T>
double TAMS<T>::getActiveTraj()
{
  return m_jj;
}
template<typename T>
void TAMS<T>::setTBranch(int tBranch)
{
  m_tBranch = tBranch;
}
template<typename T>
void TAMS<T>::setParent(int parentIdx)
{
  m_parent = parentIdx;
}

#endif
