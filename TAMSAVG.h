#ifndef _TAMS_AVG
#define _TAMS_AVG

#include "TAMS.h"

template<typename T>
class TAMSAVG : public TAMS<T>
{
 public:
  TAMSAVG(int Nc, int Nt, int NtAVG);
  TAMSAVG(int Nc, int Nt, int NtAVG, int ndof, int writingPeriod);
  ~TAMSAVG();
  void setCostFunction(double Cf, int t);
  void setCostFunction(double Cf, int t, T* ptToState);
  void writeCostFunction(std::string directoryName, int iter);
  void writeCostFunction(std::string directoryName, int iter, int k);
  virtual void computeMaxima();
  virtual double updateMaxima();
  int copyTrajToBranchingPoint();
  void readCostFunction(std::string directoryName, int iter);
 protected:
  int m_NtAVG;
  double **m_costFunctionInt;

};
template<typename T>
TAMSAVG<T>::TAMSAVG(int Nc, int Nt, int NtAVG) : TAMS<T>(Nc, Nt), m_NtAVG(NtAVG)
{
  m_costFunctionInt = new double*[Nc];
  for (int j=0;j<Nc;j++)
    {
      m_costFunctionInt[j] = new double[Nt-NtAVG+1];
      m_costFunctionInt[j][0] = 0.0;
    }
}
template<typename T>
TAMSAVG<T>::TAMSAVG(int Nc, int Nt, int NtAVG, int ndof, int writingPeriod) : TAMS<T>(Nc, Nt, ndof, writingPeriod), m_NtAVG(NtAVG)
{
  /*This version of the constructor is to be used when the dynamics have so much degrees of freedom 
that it is impossible to keep track of the states in memory. In this case, states are written/read 
from disk*/
  m_costFunctionInt = new double*[Nc];
  for (int j=0;j<Nc;j++)
    {
      m_costFunctionInt[j] = new double[Nt-NtAVG+1];
      m_costFunctionInt[j][0] = 0.0;
    }
}
template<typename T>
TAMSAVG<T>::~TAMSAVG()
{
  for(int j=0;j<this->m_Nc;j++)
    {
      delete[] m_costFunctionInt[j];
    }
  delete[] m_costFunctionInt;
}
template<typename T>
void TAMSAVG<T>::setCostFunction(double Cf, int t)
{
  double integral;
  int jj = (this->m_jj);
  int tt;
  if(t<m_NtAVG)
    {
      m_costFunctionInt[jj][0] += Cf - (this->m_costFunction)[jj][t];
      (this->m_amax)[jj] = m_costFunctionInt[jj][0];
    }
  else
    {
      tt = t-m_NtAVG + 1;
      integral =m_costFunctionInt[jj][tt-1] - (this->m_costFunction)[jj][t-m_NtAVG] + Cf;
      m_costFunctionInt[jj][tt] = integral;
      if(integral>(this->m_amax)[jj])
	{
	  (this->m_amax[jj]) = integral;
	}
    }
  TAMS<T>::setCostFunction(Cf, t);
}
template<typename T>
void TAMSAVG<T>::setCostFunction(double Cf, int t, T* ptToState)
{
  double integral;
  int jj = (this->m_jj);
  int tt;
  if(t<m_NtAVG)
    {
      m_costFunctionInt[jj][0] += Cf - (this->m_costFunction)[jj][t];
      (this->m_amax)[jj] = m_costFunctionInt[jj][0];
      if(t==m_NtAVG-1)
	{
	  if(t-(this->m_lastWritingTime) > (this->m_writingPeriod))
	    {
	      (this->writeState)(ptToState, t);
	    }
	}
    }
  else
    {
      tt = t-m_NtAVG + 1;
      integral =m_costFunctionInt[jj][tt-1] - (this->m_costFunction)[jj][t-m_NtAVG] + Cf;
      m_costFunctionInt[jj][tt] = integral;
      if(integral>(this->m_amax)[jj])
	{
	  (this->m_amax)[jj] = integral;
	  if(t-(this->m_lastWritingTime) > (this->m_writingPeriod))
	    {
	      (this->writeState)(ptToState, t);
	    }
	}
    }
  TAMS<T>::setCostFunction(Cf, t);
}
template<typename T>
void TAMSAVG<T>::writeCostFunction(std::string directoryName, int iter)
{
  TAMS<T>::writeCostFunction(directoryName, iter);
  std::ofstream trajOutputFile;
  std::stringstream fileName;
  std::string fullFileName;
  int lengthAvgTraj = (this->m_Nt) - m_NtAVG + 1;
  for(int j=0;j<(this->m_Nc);j++)
    {
      fileName << "traj_" << j << "_costFuncInt_iter_" << iter << ".dat";
      fullFileName = directoryName + fileName.str();
      trajOutputFile.open(fullFileName.c_str(), std::ios::binary);
      trajOutputFile.write((char*)&m_costFunctionInt[j][0], lengthAvgTraj*sizeof(double));
      trajOutputFile.close();
      fileName.str(std::string());
      fileName.clear();
    }
}
template<typename T>
void TAMSAVG<T>::writeCostFunction(std::string directoryName, int iter, int k)
{
  TAMS<T>::writeCostFunction(directoryName, iter, k);
  std::ofstream trajOutputFile;
  std::stringstream fileName;
  std::string fullFileName;
  int j = (this->m_idx)[k];
  int lengthAvgTraj = (this->m_Nt) - m_NtAVG + 1;
  fileName << "traj_" << j << "_costFuncInt_iter_" << iter << ".dat";
  fullFileName = directoryName + fileName.str();
  trajOutputFile.open(fullFileName.c_str(), std::ios::binary);
  trajOutputFile.write((char*)&m_costFunctionInt[j][0], lengthAvgTraj*sizeof(double));
  trajOutputFile.close();
  fileName.str(std::string());
  fileName.clear();
}
template<typename T>
void TAMSAVG<T>::computeMaxima()
{
  int Nc = this->m_Nc;
  int lengthAvgTraj = (this->m_Nt-m_NtAVG+1); 
  double amax;
  for (int j=0;j<Nc;j++)
    {
      amax = m_costFunctionInt[j][0];
      for(int t=1;t<lengthAvgTraj;t++)
	{
	  if(m_costFunctionInt[j][t]>amax){amax = m_costFunctionInt[j][t];}
	}
      (this->m_amax)[j] = amax;
    }
  (this->computeZ)();
}
template<typename T>
double TAMSAVG<T>::updateMaxima()
{
  double amax;
  int jj = this->m_jj;
  int lengthAvgTraj = (this->m_Nt-m_NtAVG+1); 
  amax = m_costFunctionInt[jj][0];
  for(int t=1;t<lengthAvgTraj;t++)
    {
      if(m_costFunctionInt[jj][t]>amax){amax = m_costFunctionInt[jj][t];}
    }
  (this->m_amax)[jj] = amax;
  (this->computeZ)();
  return amax;
}
template<typename T>
void TAMSAVG<T>::readCostFunction(std::string directoryName, int iter)
{
  TAMS<T>::readCostFunction(directoryName, iter);
  int tt;
  for(int j=0;j<this->m_Nc;j++)
    {
      for(int t=0;t<m_NtAVG;t++){m_costFunctionInt[j][0]+=(this->m_costFunction)[j][t];}
      for(int t=m_NtAVG;t<this->m_Nt;t++)
	{
	  tt = t-m_NtAVG+1;
	  m_costFunctionInt[j][tt] = m_costFunctionInt[j][tt-1]
	    -(this->m_costFunction)[j][t-m_NtAVG]+(this->m_costFunction)[j][t];
	}
    }
}
#endif
