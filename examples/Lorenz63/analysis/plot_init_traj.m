clear all
cVec = {'b', 'g', 'k', 'r'}
figure(1)
hold on
for i=1:4
  fid = fopen(['../traj_' num2str(i-1) '_iter_0.dat'], 'r');
  f = fread(fid, 30003, 'double');
  fclose(fid);
  fr = reshape(f, [3 10001]);
  x = fr(1,:);
  amax(i) = max(x);
  plot(x, cVec{i})
end
Z = min(amax);

plot(Z*ones(1,length(x)), '-r');
legend({'Traj. 0', 'Traj. 1', 'Traj. 2', 'Traj. 3', 'Niveau Z'})

