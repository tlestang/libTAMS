#include <cstdlib>
#include <cmath>
#include "LorenzDynamics.h"

LorenzDynamics::LorenzDynamics(double dt, double eps) : m_dt(dt)
{
  m_sigma = 10.0;
  m_rho = 28.0;
  m_beta = 8./3.;

  m_point.x = 1.0;
  m_point.y = 0.0;
  m_point.z = 0.0;

  m_noiseFactor = sqrt(2.*eps*m_dt);
}
LorenzDynamics::~LorenzDynamics()
{
  
}
void LorenzDynamics::advanceOneTimestep()
{
  double x,y,z;
  x = m_point.x + m_dt*m_sigma*(m_point.y-m_point.x)
    + m_noiseFactor*randNormal(0.0,1.0);
  y = m_point.y + m_dt*(m_point.x*(m_rho-m_point.z) - m_point.y)
    + m_noiseFactor*randNormal(0.0,1.0);
  z = m_point.z + m_dt*(m_point.x*m_point.y - m_beta*m_point.z)
    + m_noiseFactor*randNormal(0.0,1.0);

  m_point.x = x;
  m_point.y = y;
  m_point.z = z;
}
void LorenzDynamics::init(struct point3D point)
{
  m_point = point;
}
void LorenzDynamics::makeTransient(struct point3D x0, int Nt)
{
  init(x0);
  for(int t=0;t<Nt;t++)
    {
      advanceOneTimestep();
    }
}
struct point3D LorenzDynamics::getState()
{
  return m_point;
}

void LorenzDynamics::makePerturbation(double eps)
{
  double dnorm, norm, renorm;
  double dx, dy, dz;
  
  dx = (double) rand() / RAND_MAX;
  dy = (double) rand() / RAND_MAX;
  dz = (double) rand() / RAND_MAX;

  dnorm = sqrt(dx*dx + dy*dx + dz*dz);
  norm = sqrt(m_point.x*m_point.x+m_point.y*m_point.y+m_point.z*m_point.z);
  renorm = (norm*eps)/dnorm;

  dx *= renorm;
  dy *= renorm;
  dz *= renorm;

  m_point.x += dx;
  m_point.y += dy;
  m_point.z += dz;
}
double LorenzDynamics::randNormal(const double mean_, const double sigma_)
{
  /* Return a random number sampled in N(mean_, sigma_).
     Box-Muller method.
  */

  double x1, x2, w;
  do {
    x1 = 2.0 * (rand () / double (RAND_MAX)) - 1.0;
    x2 = 2.0 * (rand () / double (RAND_MAX)) - 1.0;
    w = x1 * x1 + x2 * x2;
  } while (w >= 1.0);

  w = sqrt (-2.0 * log (w)/w);
  const double y1 = x1 * w;
  const double y2 = x2 * w;

  return mean_ + y1 * sigma_;
}
