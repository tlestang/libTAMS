#ifndef _LORENZ_63
#define _LORENZ_63

struct point3D{
  double x;
  double y;
  double z;
};

class LorenzDynamics{
 public:
  LorenzDynamics(double dt, double eps);
  ~LorenzDynamics();
  void advanceOneTimestep();
  struct point3D getState();
  void init(struct point3D point);
  void makePerturbation(double eps);
  void makeTransient(struct point3D x0, int Nt);
  double randNormal(const double, const double);
  
 private:
  struct point3D m_point;
  double m_dt;
  double m_sigma;
  double m_rho;
  double m_beta;
  double m_noiseFactor;
  double m_eps;
};
#endif 
