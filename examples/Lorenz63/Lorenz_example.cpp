#include <iostream>
#include <cmath>
#include "TAMSAVG.h"
#include "makeSelection.h"
#include "drawParent.h"
#include "copyTrajToBranchingPoint.h"
#include "./dynamics/LorenzDynamics.h"


using namespace std;

int main()
{
  int Nc = 4; // Number of initial traj.
  double dt = 0.001; // Discretization timestep
  double T_a = 10; // Duration of the traj.
  int Nt = floor(T_a/dt)+1; // Number of timesteps
  
  // Construction of objects for TAMS and underlying dynamics
  TAMS<struct point3D>* myTAMS = new TAMS<struct point3D>(Nc, Nt);
  double eps = 0.0; // Noise amplitude in the Lorenz63 dynamics.
                    // 0. --> original deterministic dynamics.
  LorenzDynamics* myLorenz = new LorenzDynamics(dt, eps);

  struct point3D x0, x;
  string dirName = "./";
  int Niter = 200; // Number of TAMS iterations
  int nrep = 1; // Number of times the whole TAMS will be repeated.

  /* Containers for the maximum over resampled (or pruned) trajectories
     and statistical weight of the corresponding trajectory */
  vector<double> amax;
  vector<double> weights;

  int K, tBranch;
  double Z;
  double w = 1;
  double unSurN = 1./Nc;
  
  // loop over repetitions (nrep)
  for (int iCount=0;iCount<nrep;iCount++)
    {
      amax.clear();
      weights.clear();
      // TAMS init. step
      for(int j=0;j<Nc;j++)
	{
	  // Draws initial point uniformly
	  // in the [0;1]x[0;1]x[0;1] cube
	  x0.x = rand() / (double) RAND_MAX;
	  x0.y = rand() / (double) RAND_MAX;
	  x0.z = rand() / (double) RAND_MAX;

	  myLorenz->makeTransient(x0, 10000);

	  myTAMS->setActiveTraj(j);
	  for(int t=0;t<Nt;t++)
	    {
	      myLorenz->advanceOneTimestep();
	      x = myLorenz->getState();
	      myTAMS->setCostFunction(x.x, t);
	      myTAMS->setState(x, t);
	    }
	}
#ifdef _TRAJ_IO
      // writeTrajectories(string dirName, int iter) writes
      // the Nc current trajectories on disk.
      // dirName: Name of the directory the trajectories will be written in.
      // iter: Index of the iteration corresponding to the traj.
      // 	0 is initialization.
      myTAMS->writeTrajectories(dirName, 0);
#endif
      /* The computeMaxima() method computes the maximum over each trajectory
	 and updates the value of the threshold Z */
      myTAMS->computeMaxima();
      /*
	In this example we gather the max and weight for the 
	- Nc initial trajectories.
	- \tilde{J} resampled trajectories.
       */
      for(int j=0;j<Nc;j++)
	{
	  amax.push_back(myTAMS->getMax(j));
	  weights.push_back(w);
	}
      
      // Loop over iterations of the TAMS algorithm
      for (int i=0;i<Niter;i++)
	{
	  /* Selects the K trajectories which maximum corresponds 
             to the threshold Z */
	  K = myTAMS->makeSelection();
	  if(K>=Nc)
	    {
	      Z = myTAMS->getZ();    
	      cout << "EXTINCTION at iteration " << i << endl;
	      cout << "Threshold is Z = " << Z << endl;
	      return 0;
	    }
	  
	  /* Compute the statistical weight corresponding to 
             the K traj. selected traj. */
	  w = w*(1.-K*unSurN);	  
	  /* Now for each one of these traj., do: */
	  for (int k=0; k<K;k++)
	    {
	      // Picks a parent traj among the (Nc-K) remaining traj.
	      myTAMS->drawParent();
	      myTAMS->setActiveTraj(k); // Mark the trajectory as ACTIVE
	      
	      /* Parent traj. overwrites the selected traj.
                 until branching point*/
	      tBranch = myTAMS->copyTrajToBranchingPoint();
	      /* Get the restart state, in this case a single struct Point3D,
                 into buffer x0 */
	      x0 = myTAMS->getRestart();
	      myLorenz->init(x0);
#ifdef _PERTURB
	      // if the _PERTURB compilation option is used,
	      // the restart state is slightly perturbed.
	      myLorenz->makePerturbation(0.01);
#endif
	      /* Compute dynamics from tBranch to T_a */
	      for (int t=tBranch+1;t<Nt;t++)
		{
		  myLorenz->advanceOneTimestep();
		  x = myLorenz->getState();
		  myTAMS->setCostFunction(x.x, t); // Updates cost function
		  // In this example trajectories are kept in memory.
		  // Recall that x is a struct Point3D
		  myTAMS->setState(x, t);
		}
	      /*Go through the whole current active trajectory 
                and compute maximum. If needed, the threshold Z
                is updated */
	      myTAMS->updateMaxima();
	      /* In this case we record the max and weight of the
		 __resampled__ traj.*/
	      amax.push_back(myTAMS->getMax());
	      weights.push_back(w);
	    }
#ifdef _TRAJ_IO
	  myTAMS->writeTrajectories(dirName, i+1);
#endif
	}
      /* 
	 End of iterations.
	 Writes arrays to disk.
      */
      ofstream amaxfile("amax.dat", ios::binary | ios::app);
      amaxfile.write((char*)&amax[0], amax.size()*sizeof(double));
      amaxfile.close();
      ofstream wfile("weights.dat", ios::binary | ios::app);
      wfile.write((char*)&weights[0], weights.size()*sizeof(double));
      wfile.close();
    }
  /*
    End of repetitions.
    
  */
  delete myTAMS;
  delete myLorenz;
}
