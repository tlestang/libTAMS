#include <iostream>
#include <cmath>
#include <cfloat>
#include <mpi.h>

#include "TAMSAVG.h"
#include "makeSelection.h"
#include "drawParent.h"
#include "copyTrajToBranchingPoint.h"

#include "libpipeLBM.h"

using namespace std;

int main(int argc, char* argv[])
{
  /*
    Definition of parameters for the flow dynamics
  */
  int Dx = 257;  // Nb of lattice points along x direction
  int Dy = 65; // Nb of lattice points along y direction
  int spongeStart = (int) 3*(Dx-1)/4+1; // Start of the sponge layer
  int R = (Dy-1)/16; // Size of grid elements
  int x0 = (Dx-1)/16; // Grid position
  int L = 8; // Size of the square obstacle
  int xsq = (Dx-1)/2;       // (xsq, ysq) is the location of the 
  int ysq = (Dy-1)/2 - L/2; // bottom-left corner of the obstacle
  double U0 = 0.05; // max inlet velocity (Poiseuille profile)
  double tau = 0.5005; // Relaxation time
  double F0 = 0.0; // Forcing amplitude

  // Construction of the pipeLBM object
  pipeLBM *myLB = new pipeLBM(Dx, Dy);
  myLB->setInletBC("poiseuille");
  myLB->setOutletBC("open");				    
  myLB->setSpongeLayer(spongeStart, Dx);
  myLB->setParameters(tau, U0, F0);

  // Constrution of the grid and square obstacle
  // which are both implemented by Obstacle objects
  Obstacle* obs[2];
  obs[0] = new Grid(x0, R, Dy);
  obs[1] = new squareObstacle(xsq, ysq, L);
  myLB->setObstacles(obs, 2);

  // Drag correlation time in unit of timesteps.
  // It is determined experimentally.
  int correlationTime = 1000;

  /*
    MPI init.
  */

  MPI_Status status;
  int my_rank, nb_ranks, tag=0, error;
  MPI_Init(&argc, &argv);
  MPI_Comm_rank(MPI_COMM_WORLD,&my_rank);
  MPI_Comm_size(MPI_COMM_WORLD,&nb_ranks);
  // Fix seed for all ranks for reproducibility.  
  srand(1+my_rank);

  /* 
     Definition of parameters for the TAMS
  */
  int Nc = 2; // Number of initial traj. 
  double T_a = 5; // Duration of the traj, in units of correlation times.
  double eps = 0.002; //Amplitude of the perturbation after branching.
  // Each MPI rank works on local_Nc copies
  int local_Nc = Nc/nb_ranks; // p is the nb of MPI processes
  int stateSavingPeriod = correlationTime; 
  int Nt = T_a*correlationTime; // Each trajectory spans Nt timesteps
  
  // Construction of the TAMS object
  TAMS<double>* myTAMS = new TAMS<double>(Nc, Nt, Dx*Dy*9, stateSavingPeriod);

  /*
    Decalration of miscellaneous variables 
  */
  string dirName = "./";
  double amax;
  double Cf;
  double *ptToState;
  int tTransient = 10*correlationTime;
  int K, tBranch, tRestart;
  int tlast;

  /*
    --------- INITIALISATION OF TAMS --------- 
  */
  bool errorFlag = false;
  for(int j=my_rank*local_Nc ; j<(my_rank+1)*local_Nc ; j++)
    {
      // Each MPI rank initialise ites local_Nc traj. to a random state.
      // See documentation of libpipeLBM
      // ../libTAMS0/exemples/pipeLBM/libpipeLBM/
      errorFlag = myLB->initFromRandom(_PATH_INIT, _INIT_FILES_ROOT, 5, _NB_INIT_FILES);
      if(errorFlag)
      	{
      	  cout << "Error while init." << endl;
      	}
      // Transient simulation to damp suprious sound waves
      // emitted after init. --> relaxation towards physical state.
      for (int t=0;t<tTransient;t++){myLB->advanceOneTimestep(obs, 2);}
      
      myTAMS->setActiveTraj(j); // Mark the trajectory as ACTIVE
      myTAMS->openStateFile();  // Opens a file in dirName in order to
                                // write saved states on disk
                               
      for(int t=0;t<Nt;t++)
      	{
      	  myLB->advanceOneTimestep(obs, 2);
      	  myLB->computeStress(obs[1]);
	  Cf = obs[1]->getDrag();
	  ptToState = myLB->getState();
	  // The following overloaded setCostFunction() takes
	  // as an add. argument a pointer to the current state.
	  // If the cost function reaches a new global max., and the
	  // last timestamp was more than stateSavingPeriod timesteps ago,
	  // then the state is written on disk.
      	  myTAMS->setCostFunction(Cf, t, ptToState);
      	}
#ifdef _TRAJ_IO
      // writeCostFunction(string dirName, int iter, int j)
      // and writeTimeStamps writes
      // the cost function and timestamps along the jth trajectory.
      // dirName: Name of the directory the trajectories will be written in.
      // iter: Index of the iteration corresponding to the traj.
      // 	0 is initialization.
      myTAMS->writeCostFunction(dirName, 0, j);
      myTAMS->writeTimeStamps(dirName,0, j);
#endif      
    }

  delete myTAMS;
  delete myLB;
  delete obs[1];
  delete obs[0];

  MPI_Finalize();
}
