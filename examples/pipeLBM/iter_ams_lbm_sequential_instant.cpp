#include <iostream>
#include <cmath>
#include <cfloat>
#include <fstream>
#include <sstream>

#include "TAMSAVG.h"
#include "makeSelection.h"
#include "drawParent.h"
#include "copyTrajToBranchingPoint.h"

#include "libpipeLBM.h"

using namespace std;

int main(int argc, char* argv[])
{
   /*
    Definition of parameters for the flow dynamics
  */
  int Dx = 257;  // Nb of lattice points along x direction
  int Dy = 65; // Nb of lattice points along y direction
  int spongeStart = (int) 3*(Dx-1)/4+1; // Start of the sponge layer
  int R = (Dy-1)/16; // Size of grid elements
  int x0 = (Dx-1)/16; // Grid position
  int L = 8; // Size of the square obstacle
  int xsq = (Dx-1)/2;       // (xsq, ysq) is the location of the 
  int ysq = (Dy-1)/2 - L/2; // bottom-left corner of the obstacle
  double U0 = 0.05; // max inlet velocity (Poiseuille profile)
  double tau = 0.5005; // Relaxation time
  double F0 = 0.0; // Forcing amplitude

  // Construction of the pipeLBM object
  pipeLBM *myLB = new pipeLBM(Dx, Dy);
  myLB->setInletBC("poiseuille");
  myLB->setOutletBC("open");				    
  myLB->setSpongeLayer(spongeStart, Dx);
  myLB->setParameters(tau, U0, F0);

  // Constrution of the grid and square obstacle
  // which are both implemented by Obstacle objects
  Obstacle* obs[2];
  obs[0] = new Grid(x0, R, Dy);
  obs[1] = new squareObstacle(xsq, ysq, L);
  myLB->setObstacles(obs, 2);

  // Drag correlation time in unit of timesteps.
  // It is determined experimentally.
  int correlationTime = 1000;

  /* 
     Definition of parameters for the TAMS
  */
  int Nc = 2; // Number of initial traj. 
  double T_a = 5; // Duration of the traj, in units of correlation times.
  double Niter=200;
  double eps = 0.002; //Amplitude of the perturbation after branching.
  int stateSavingPeriod = correlationTime;
  int updatePeriod = 50;
  int restartPeriod = 500;
  int Nt = T_a*correlationTime; // Each trajectory spans Nt timesteps
  
  // Construction of the TAMS object
  TAMS<double>* myTAMS = new TAMS<double>(Nc, Nt, Dx*Dy*9, stateSavingPeriod);

  /*
    Decalration of miscellaneous variables 
  */
  string dirName = "./";
  double Cf;
  double *ptToState;
  int K;
  double Z;
  int tBranch;
  int tRestart;
  int tlast;

  string instru;
  stringstream dumss;
  ofstream lulu;

  /* Containers for the maximum over resampled (or pruned) trajectories
     index of selected and parent traj and number of selected traj. */
  // In this example the weights are not recorded as they can be computed
  // a posteriori
  vector<double> amax;
  vector<int> selectedTraj;
  vector<int> parentTraj;
  vector<int> nbSelectedTraj;

  // Reads in the cost function
  // and timestamps from disk
  myTAMS->readCostFunction(dirName, 0);
  myTAMS->readTimeStamps(dirName, 0);
  myTAMS->computeMaxima();

  bool errorFlag = false;
  for (int j=0;j<Niter;j++)
    {
      /* Selects the K trajectories which maximum corresponds 
	 to the threshold Z */
      K = myTAMS->makeSelection();
      if(K>=Nc)
	{
	  Z = myTAMS->getZ();    
	  cout << "EXTINCTION at iteration " << j << endl;
	  cout << "Threshold is Z = " << Z << endl;
	  return 0;
	}
      /* Now for each one of these traj., do: */
      for(int k=0;k<K;k++)
	{
	  // Picks a parent traj among the (Nc-K) remaining traj.
	  myTAMS->drawParent();
	  myTAMS->setActiveTraj(k); // Mark the trajectory as ACTIVE
	  
	  /* Parent traj. overwrites the selected traj.
	     until branching point*/
	  tBranch = myTAMS->copyTrajToBranchingPoint();
	  // Gets the address of the state array
	  //  [0][][][][][][][]...[][][][][ndof-1]
	  //   ^
	  // ptToState 
	  ptToState = myLB->getState();
	  // The corresponding memory locations are overwritten 
	  // with the restart state
	  tRestart = myTAMS->getRestart(ptToState);
	  // Crucial call to pipeLBM::initMacro
	  // See documentation of pipeLBM
	  // ../libTAMS0/exemples/pipeLBM/libpipeLBM/
	  myLB->initMacro();
	  // Simulation of the dynamics from the restart state
	  // to he branching point.
	  for(int t=tRestart;t<tBranch;t++)
	    {
	      myLB->advanceOneTimestep(obs, 2);
	    }

	  // A timestamp for the branching state is generated.
	  myTAMS->openStateFile();
	  // Write branching state on disk.
	  ptToState = myLB->getState();
	  myTAMS->writeState(ptToState, tBranch); // Records timestamp

	  // Perturbation of the state at tBranch+1 
	  myLB->advanceOneTimestep(obs, 2); // Advance from tBranch to tBranch+1
	  errorFlag = myLB->makePerturbation(_PATH_INIT, _INIT_FILES_ROOT, 5, _NB_INIT_FILES, eps);
	  // The perturbed state at tBranch+1 *must* be timestamped as well.
	  ptToState = myLB->getState();
	  myTAMS->writeState(ptToState, tBranch+1);
	  
	  // Update of the cost function after perturbation
	  myLB->computeStress(obs[1]);
	  Cf = obs[1]->getDrag();
	  myTAMS->setCostFunction(Cf, tBranch+1);

	  // Write resampling information in std output
	  cout << "  Iteration " << j << " : Resampling copy " << myTAMS->getActiveTraj()
	       << " from t = " << tBranch/(double)correlationTime << " from parent "
	       << myTAMS->getParentTraj()<<endl;
	  // Record the index of selected and parent traj.
	  selectedTraj.push_back( myTAMS->getActiveTraj() );
	  parentTraj.push_back( myTAMS->getParentTraj() );

	  /* Compute dynamics from tBranch+2 to T_a */
	  for(int t=tBranch+2;t<Nt;t++)
	    {
	      myLB->advanceOneTimestep(obs, 2);
	      myLB->computeStress(obs[1]);
	      Cf = obs[1]->getDrag();
	      ptToState = myLB->getState();
	      myTAMS->setCostFunction(Cf, t, ptToState);
	    }
	  // Records the max over the resampled traj.
	  amax.push_back(myTAMS->getMax());

#ifdef _TRAJ_IO
	  myTAMS->writeCostFunction(dirName, j, k);
	  myTAMS->writeTimeStamps(dirName, j, k);
#endif
	}
      /*
	End of loop over the K selected traj.
      */
      if(j%restartPeriod==0)
	{
	  // Every restartPeriod iterations, a
	  // "restart directory" is created.
	  dumss << "restart_iter_" << j << endl;
	  instru = "mkdir " + dumss.str();
	  system(instru.c_str());
	  myTAMS->writeCostFunction(dumss.str(), j);
	  myTAMS->writeTimeStamps(dumss.str(), j);
	  dumss.str(string());
	  dumss.clear();
	}
      if(j%updatePeriod==0)
	{
	  cout << "  Updating output" << endl;
	  lulu.open("amax.dat", ios::binary | ios::app);
	  lulu.write((char*)&amax[0], amax.size()*sizeof(double));
	  lulu.close();
	  lulu.open("selectedTraj.dat", ios::binary | ios::app);
	  lulu.write((char*)&selectedTraj[0], selectedTraj.size()*sizeof(int));
	  lulu.close();
	  lulu.open("parentTraj.dat", ios::binary | ios::app);
	  lulu.write((char*)&parentTraj[0], parentTraj.size()*sizeof(int));
	  lulu.close();
	  lulu.open("nbSelectedTraj.dat", ios::binary | ios::app);
	  lulu.write((char*)&nbSelectedTraj[0], nbSelectedTraj.size()*sizeof(int));
	  lulu.close();
	  
	  amax.clear();
	  selectedTraj.clear();
	  parentTraj.clear();
	}
      // Records the number of selected traj. for this iteration.
      nbSelectedTraj.push_back(K);
    }
  /*
    End of iterations j
  */

  // Write containers on disk
  lulu.open("amax.dat", ios::binary | ios::app);
  lulu.write((char*)&amax[0], amax.size()*sizeof(double));
  lulu.close();
  lulu.open("selectedTraj.dat", ios::binary | ios::app);
  lulu.write((char*)&selectedTraj[0], selectedTraj.size()*sizeof(int));
  lulu.close();
  lulu.open("parentTraj.dat", ios::binary | ios::app);
  lulu.write((char*)&parentTraj[0], parentTraj.size()*sizeof(int));
  lulu.close();
  lulu.open("nbSelectedTraj.dat", ios::binary | ios::app);
  lulu.write((char*)&nbSelectedTraj[0], nbSelectedTraj.size()*sizeof(int));
  lulu.close();

  delete myTAMS;
  delete myLB;
  delete obs[1];
  delete obs[0];
}
