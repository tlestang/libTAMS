#include "pipeLBM.h"
#include <iostream>

void pipeLBM::doStreamingAndCollision()
{
  int c[9][2] = {{0,0}, {1,0}, {0,1}, {-1,0}, {0,-1}, {1,1}, {-1,1}, {-1,-1}, {1,-1}};
  double w[9]={4.0/9.0, 1.0/9.0, 1.0/9.0, 1.0/9.0, 1.0/9.0, 1.0/36.0, 1.0/36.0, 1.0/36.0, 1.0/36.0};
  double omega = 1./m_tau;
  double omega_vect[9] = {omega, 1, 1, 1, omega, omega, 1, 1, 1};
  double omega_vect1[9] = {1-omega, 0, 0, 0, 1-omega, 1-omega, 0, 0, 0};
  double collision_op[9];
  double rho_, fk;
  double cs2 = 1./3.;
  double nu = cs2*(m_tau-0.5);
  double ux_, uy_,ux_2,uy_2;  double eu, eueu, u2, feq;
  double omega1 = 1. - omega;
  double l, om, om1;
  int nx, ny;
  double k0,k1,k2,k3,k4,k5,k6,k7,k8;
  double k0_eq,k1_eq,k2_eq,k3_eq,k4_eq,k5_eq,k6_eq,k7_eq,k8_eq;
  double k1_neq,k2_neq,k3_neq,k4_neq,k5_neq;
  double k1_star,k2_star,k3_star,k4_star,k5_star,k6_star,k7_star,k8_star;
  double CX, CY;
  double Fy = 0.0;
  double Fx = m_F0;
  
  for(int x=0;x<m_spgeStart;x++)
    {
      for(int y=0;y<m_Dy;y++)
	{
	  
	  if(isFluid[x][y])
	    {
	      ux_ = ux[idx(x,y)]; uy_ = uy[idx(x,y)];
	      rho_ = rho[idx(x,y)];
	      ux_2 = ux_*ux_;
	      uy_2 = uy_*uy_;
	      //k0 = rho_; k0_eq = rho_;
	      k1=k2=k3=k4=k5=k6=k7=k8=0.0;
	      k3_eq=k4_eq=k5_eq=0.0;
	      u2 = ux_*ux_ + uy_*uy_;
	      for(int k=0;k<9;k++)
		{
		  CX = c[k][0]-ux_; CY = c[k][1]-uy_;
		  // eu = c[k][0]*ux_ + c[k][1]*uy_;
		  // eueu = 4.5*eu*eu;
	      	      
		  fk = fin[IDX(x,y,k)];
		  /*Compute central moments */
		  // k1 += fk*CX;
		  // k2 += fk*CY;
		  k3 += fk*(CX*CX+CY*CY);
		  k4 += fk*(CX*CX-CY*CY);
		  k5 += fk*CX*CY;
		  k6 += fk*CX*CX*CY;
		  k7 += fk*CX*CY*CY;
		  k8 += fk*CX*CX*CY*CY;
		}
	      /* Compute equilibrium centered moments*/
	      k1_eq = 3.*nu*Fx;
	      k2_eq = 3.*nu*Fy;
	      k3_eq = 2.*rho_*cs2;
	      k4_eq = 0.;
	      k5_eq = 0.;
	      k6_eq = -rho_*ux_*ux_*uy_+nu*(Fy-3.*Fy*ux_*ux_-6.*Fx*ux_*uy_);
	      k7_eq = -rho_*ux_*uy_*uy_+nu*(Fx-3.*Fx*uy_*uy_-6.*Fy*ux_*uy_);
	      k8_eq = rho_*cs2*cs2*(27.*ux_*ux_*uy_*uy_+1.)+12.*nu*ux_*uy_*(Fx*uy_+Fy*ux_);

	      /*Collision for 2nd order centered moments*/
	      k1_star = omega_vect1[1]*k1+omega_vect[1]*k1_eq;
	      k2_star = omega_vect1[2]*k2+omega_vect[2]*k2_eq;
	      k3_star = omega_vect1[3]*k3+omega_vect[3]*k3_eq;
	      k4_star = omega_vect1[4]*k4+omega_vect[4]*k4_eq;
	      k5_star = omega_vect1[5]*k5+omega_vect[5]*k5_eq;
	      k6_star = omega_vect1[6]*k6+omega_vect[6]*k6_eq;
	      k7_star = omega_vect1[7]*k7+omega_vect[7]*k7_eq;		 		 		 
	      k8_star = omega_vect1[8]*k8+omega_vect[8]*k8_eq;

	      collision_op[0] = k1_star*(-2*ux_+2*ux_*uy_2)+k2_star*(-2*uy_+2*ux_2*uy_)+k3_star*(-1+.5*ux_2+.5*uy_2)+k4_star*(-ux_2+uy_2)+4*ux_*uy_*k5_star+2*uy_*k6_star+2*ux_*k7_star+k8_star+rho_*(1-ux_2-uy_2+ux_2*uy_2);
	      collision_op[1] = k1_star*(0.5+ux_-0.5*uy_2-ux_*uy_2)+k2_star*(-ux_*uy_-ux_2*uy_)+0.25*k3_star*(1-ux_-ux_2-uy_2)+0.25*k4_star*(1+ux_+ux_2-uy_2)+k5_star*(-uy_-2*ux_*uy_)-uy_*k6_star+k7_star*(-.5-ux_)-.5*k8_star+0.5*rho_*(ux_+ux_2-ux_*uy_2-ux_2*uy_2);
	      collision_op[2] = k1_star*(-ux_*uy_-ux_*uy_2)+k2_star*(0.5+uy_-0.5*ux_2-ux_2*uy_)+0.25*k3_star*(1-uy_-ux_2-uy_2)+0.25*k4_star*(-1-uy_+ux_2-uy_2)+k5_star*(-ux_-2*ux_*uy_)+k6_star*(-.5-uy_)-ux_*k7_star-.5*k8_star+0.5*rho_*(uy_+uy_2-ux_2*uy_-ux_2*uy_2);
	      collision_op[3] = k1_star*(-0.5+ux_+0.5*uy_2-ux_*uy_2)+k2_star*(ux_*uy_-ux_2*uy_)+0.25*k3_star*(1+ux_-ux_2-uy_2)+0.25*k4_star*(1-ux_+ux_2-uy_2)+k5_star*(uy_-2*ux_*uy_)-uy_*k6_star+k7_star*(.5-ux_)-.5*k8_star+0.5*rho_*(-ux_+ux_2+ux_*uy_2-ux_2*uy_2);
	      collision_op[4] = k1_star*(ux_*uy_-ux_*uy_2)+k2_star*(-0.5+uy_+0.5*ux_2-ux_2*uy_)+0.25*k3_star*(1+uy_-ux_2-uy_2)+0.25*k4_star*(-1+uy_+ux_2-uy_2)+k5_star*(ux_-2*ux_*uy_)+k6_star*(.5-uy_)-ux_*k7_star-.5*k8_star+0.5*rho_*(-uy_+uy_2+ux_2*uy_-ux_2*uy_2);
	      collision_op[5] = 0.25*k1_star*(uy_+uy_2+2*ux_*uy_+2*ux_*uy_2)+0.25*k2_star*(ux_+ux_2+2*ux_*uy_+ux_2*uy_)+0.125*k3_star*(ux_+uy_+ux_2+uy_2)+0.125*k4_star*(-ux_+uy_-ux_2+uy_2)+k5_star*(0.25+.5*ux_+.5*uy_+ux_*uy_)+k6_star*(.25+.5*uy_)+k7_star*(.25+.5*ux_)+.25*k8_star+.25*rho_*(ux_*uy_+ux_2*uy_+ux_*uy_2+ux_2*uy_2);
	      collision_op[6] = 0.25*k1_star*(-uy_-uy_2+2*ux_*uy_+2*ux_*uy_2)+0.25*k2_star*(-ux_+ux_2-2*ux_*uy_+ux_2*uy_)+0.125*k3_star*(-ux_+uy_+ux_2+uy_2)+0.125*k4_star*(ux_+uy_-ux_2+uy_2)+k5_star*(-0.25+.5*ux_-.5*uy_+ux_*uy_)+k6_star*(.25+.5*uy_)+k7_star*(-.25+.5*ux_)+.25*k8_star+.25*rho_*(-ux_*uy_+ux_2*uy_-ux_*uy_2+ux_2*uy_2);
	      collision_op[7] = 0.25*k1_star*(uy_-uy_2-2*ux_*uy_+2*ux_*uy_2)+0.25*k2_star*(ux_-ux_2-2*ux_*uy_+ux_2*uy_)+0.125*k3_star*(-ux_-uy_+ux_2+uy_2)+0.125*k4_star*(ux_-uy_-ux_2+uy_2)+k5_star*(0.25-.5*ux_-.5*uy_+ux_*uy_)+k6_star*(-.25+.5*uy_)+k7_star*(-.25+.5*ux_)+.25*k8_star+.25*rho_*(ux_*uy_-ux_2*uy_-ux_*uy_2+ux_2*uy_2);
	      collision_op[8] = 0.25*k1_star*(-uy_+uy_2-2*ux_*uy_+2*ux_*uy_2)+0.25*k2_star*(-ux_-ux_2+2*ux_*uy_+ux_2*uy_)+0.125*k3_star*(ux_-uy_+ux_2+uy_2)+0.125*k4_star*(-ux_-uy_-ux_2+uy_2)+k5_star*(-0.25-.5*ux_+.5*uy_+ux_*uy_)+k6_star*(-.25+.5*uy_)+k7_star*(.25+.5*ux_)+.25*k8_star+.25*rho_*(-ux_*uy_-ux_2*uy_+ux_*uy_2+ux_2*uy_2);
				 
	      for(int k=0;k<9;k++)
		{
		  nx = (x + c[k][0]+m_Dx)%m_Dx;
		  ny = (y + c[k][1]+m_Dy)%m_Dy;
		  fin[IDX(x,y,k)] = collision_op[k];
		  fout[IDX(nx,ny,k)] = collision_op[k];
		}
	    }
	}
    }


  for(int x=m_spgeStart;x<m_spgeEnd;x++)
    {
      l =(x-m_spgeStart)/(double)(m_spgeEnd-1-m_spgeStart);
      om = (1-0.999*l*l)*omega;
      omega_vect[0] = om;
      omega_vect[4] = om;
      omega_vect[5] = om;
      omega_vect1[0] = 1-om;
      omega_vect1[4] = 1-om;
      omega_vect1[5] = 1-om;
      for(int y=0;y<m_Dy;y++)
	{
	  
	  if(isFluid[x][y])
	    {
	      ux_ = ux[idx(x,y)]; uy_ = uy[idx(x,y)];
	      rho_ = rho[idx(x,y)];
	      ux_2 = ux_*ux_;
	      uy_2 = uy_*uy_;
	      //k0 = rho_; k0_eq = rho_;
	      k1=k2=k3=k4=k5=k6=k7=k8=0.0;
	      k3_eq=k4_eq=k5_eq=0.0;
	      u2 = ux_*ux_ + uy_*uy_;
	      for(int k=0;k<9;k++)
		{
		  CX = c[k][0]-ux_; CY = c[k][1]-uy_;
		  // eu = c[k][0]*ux_ + c[k][1]*uy_;
		  // eueu = 4.5*eu*eu;
	      	      
		  fk = fin[IDX(x,y,k)];
		  /*Compute central moments */
		  // k1 += fk*CX;
		  // k2 += fk*CY;
		  k3 += fk*(CX*CX+CY*CY);
		  k4 += fk*(CX*CX-CY*CY);
		  k5 += fk*CX*CY;
		  k6 += fk*CX*CX*CY;
		  k7 += fk*CX*CY*CY;
		  k8 += fk*CX*CX*CY*CY;
		}
	      /* Compute equilibrium centered moments*/
	      k1_eq = 3.*nu*Fx;
	      k2_eq = 3.*nu*Fy;
	      k3_eq = 2.*rho_*cs2;
	      k4_eq = 0.;
	      k5_eq = 0.;
	      k6_eq = -rho_*ux_*ux_*uy_+nu*(Fy-3.*Fy*ux_*ux_-6.*Fx*ux_*uy_);
	      k7_eq = -rho_*ux_*uy_*uy_+nu*(Fx-3.*Fx*uy_*uy_-6.*Fy*ux_*uy_);
	      k8_eq = rho_*cs2*cs2*(27.*ux_*ux_*uy_*uy_+1.)+12.*nu*ux_*uy_*(Fx*uy_+Fy*ux_);

	      /*Collision for 2nd order centered moments*/
	      k1_star = omega_vect1[1]*k1+omega_vect[1]*k1_eq;
	      k2_star = omega_vect1[2]*k2+omega_vect[2]*k2_eq;
	      k3_star = omega_vect1[3]*k3+omega_vect[3]*k3_eq;
	      k4_star = omega_vect1[4]*k4+omega_vect[4]*k4_eq;
	      k5_star = omega_vect1[5]*k5+omega_vect[5]*k5_eq;
	      k6_star = omega_vect1[6]*k6+omega_vect[6]*k6_eq;
	      k7_star = omega_vect1[7]*k7+omega_vect[7]*k7_eq;		 		 		 
	      k8_star = omega_vect1[8]*k8+omega_vect[8]*k8_eq;

	      collision_op[0] = k1_star*(-2*ux_+2*ux_*uy_2)+k2_star*(-2*uy_+2*ux_2*uy_)+k3_star*(-1+.5*ux_2+.5*uy_2)+k4_star*(-ux_2+uy_2)+4*ux_*uy_*k5_star+2*uy_*k6_star+2*ux_*k7_star+k8_star+rho_*(1-ux_2-uy_2+ux_2*uy_2);
	      collision_op[1] = k1_star*(0.5+ux_-0.5*uy_2-ux_*uy_2)+k2_star*(-ux_*uy_-ux_2*uy_)+0.25*k3_star*(1-ux_-ux_2-uy_2)+0.25*k4_star*(1+ux_+ux_2-uy_2)+k5_star*(-uy_-2*ux_*uy_)-uy_*k6_star+k7_star*(-.5-ux_)-.5*k8_star+0.5*rho_*(ux_+ux_2-ux_*uy_2-ux_2*uy_2);
	      collision_op[2] = k1_star*(-ux_*uy_-ux_*uy_2)+k2_star*(0.5+uy_-0.5*ux_2-ux_2*uy_)+0.25*k3_star*(1-uy_-ux_2-uy_2)+0.25*k4_star*(-1-uy_+ux_2-uy_2)+k5_star*(-ux_-2*ux_*uy_)+k6_star*(-.5-uy_)-ux_*k7_star-.5*k8_star+0.5*rho_*(uy_+uy_2-ux_2*uy_-ux_2*uy_2);
	      collision_op[3] = k1_star*(-0.5+ux_+0.5*uy_2-ux_*uy_2)+k2_star*(ux_*uy_-ux_2*uy_)+0.25*k3_star*(1+ux_-ux_2-uy_2)+0.25*k4_star*(1-ux_+ux_2-uy_2)+k5_star*(uy_-2*ux_*uy_)-uy_*k6_star+k7_star*(.5-ux_)-.5*k8_star+0.5*rho_*(-ux_+ux_2+ux_*uy_2-ux_2*uy_2);
	      collision_op[4] = k1_star*(ux_*uy_-ux_*uy_2)+k2_star*(-0.5+uy_+0.5*ux_2-ux_2*uy_)+0.25*k3_star*(1+uy_-ux_2-uy_2)+0.25*k4_star*(-1+uy_+ux_2-uy_2)+k5_star*(ux_-2*ux_*uy_)+k6_star*(.5-uy_)-ux_*k7_star-.5*k8_star+0.5*rho_*(-uy_+uy_2+ux_2*uy_-ux_2*uy_2);
	      collision_op[5] = 0.25*k1_star*(uy_+uy_2+2*ux_*uy_+2*ux_*uy_2)+0.25*k2_star*(ux_+ux_2+2*ux_*uy_+ux_2*uy_)+0.125*k3_star*(ux_+uy_+ux_2+uy_2)+0.125*k4_star*(-ux_+uy_-ux_2+uy_2)+k5_star*(0.25+.5*ux_+.5*uy_+ux_*uy_)+k6_star*(.25+.5*uy_)+k7_star*(.25+.5*ux_)+.25*k8_star+.25*rho_*(ux_*uy_+ux_2*uy_+ux_*uy_2+ux_2*uy_2);
	      collision_op[6] = 0.25*k1_star*(-uy_-uy_2+2*ux_*uy_+2*ux_*uy_2)+0.25*k2_star*(-ux_+ux_2-2*ux_*uy_+ux_2*uy_)+0.125*k3_star*(-ux_+uy_+ux_2+uy_2)+0.125*k4_star*(ux_+uy_-ux_2+uy_2)+k5_star*(-0.25+.5*ux_-.5*uy_+ux_*uy_)+k6_star*(.25+.5*uy_)+k7_star*(-.25+.5*ux_)+.25*k8_star+.25*rho_*(-ux_*uy_+ux_2*uy_-ux_*uy_2+ux_2*uy_2);
	      collision_op[7] = 0.25*k1_star*(uy_-uy_2-2*ux_*uy_+2*ux_*uy_2)+0.25*k2_star*(ux_-ux_2-2*ux_*uy_+ux_2*uy_)+0.125*k3_star*(-ux_-uy_+ux_2+uy_2)+0.125*k4_star*(ux_-uy_-ux_2+uy_2)+k5_star*(0.25-.5*ux_-.5*uy_+ux_*uy_)+k6_star*(-.25+.5*uy_)+k7_star*(-.25+.5*ux_)+.25*k8_star+.25*rho_*(ux_*uy_-ux_2*uy_-ux_*uy_2+ux_2*uy_2);
	      collision_op[8] = 0.25*k1_star*(-uy_+uy_2-2*ux_*uy_+2*ux_*uy_2)+0.25*k2_star*(-ux_-ux_2+2*ux_*uy_+ux_2*uy_)+0.125*k3_star*(ux_-uy_+ux_2+uy_2)+0.125*k4_star*(-ux_-uy_-ux_2+uy_2)+k5_star*(-0.25-.5*ux_+.5*uy_+ux_*uy_)+k6_star*(-.25+.5*uy_)+k7_star*(.25+.5*ux_)+.25*k8_star+.25*rho_*(-ux_*uy_-ux_2*uy_+ux_*uy_2+ux_2*uy_2);
				 
	      for(int k=0;k<9;k++)
		{
		  nx = (x + c[k][0]+m_Dx)%m_Dx;
		  ny = (y + c[k][1]+m_Dy)%m_Dy;
		  fin[IDX(x,y,k)] = collision_op[k];
		  fout[IDX(nx,ny,k)] = collision_op[k];
		}
	    }
	}
    }

  for(int x=m_spgeEnd;x<m_Dx;x++)
    {
      om = 0.001*omega;
      omega_vect[0] = om;
      omega_vect[4] = om;
      omega_vect[5] = om;
      omega_vect1[0] = 1-om;
      omega_vect1[4] = 1-om;
      omega_vect1[5] = 1-om;
      for(int y=0;y<m_Dy;y++)
	{
	  
	  if(isFluid[x][y])
	    {
	      ux_ = ux[idx(x,y)]; uy_ = uy[idx(x,y)];
	      rho_ = rho[idx(x,y)];
	      ux_2 = ux_*ux_;
	      uy_2 = uy_*uy_;
	      //k0 = rho_; k0_eq = rho_;
	      k1=k2=k3=k4=k5=k6=k7=k8=0.0;
	      k3_eq=k4_eq=k5_eq=0.0;
	      u2 = ux_*ux_ + uy_*uy_;
	      for(int k=0;k<9;k++)
		{
		  CX = c[k][0]-ux_; CY = c[k][1]-uy_;
		  // eu = c[k][0]*ux_ + c[k][1]*uy_;
		  // eueu = 4.5*eu*eu;
	      	      
		  fk = fin[IDX(x,y,k)];
		  /*Compute central moments */
		  // k1 += fk*CX;
		  // k2 += fk*CY;
		  k3 += fk*(CX*CX+CY*CY);
		  k4 += fk*(CX*CX-CY*CY);
		  k5 += fk*CX*CY;
		  k6 += fk*CX*CX*CY;
		  k7 += fk*CX*CY*CY;
		  k8 += fk*CX*CX*CY*CY;
		}
	      /* Compute equilibrium centered moments*/
	      k1_eq = 3.*nu*Fx;
	      k2_eq = 3.*nu*Fy;
	      k3_eq = 2.*rho_*cs2;
	      k4_eq = 0.;
	      k5_eq = 0.;
	      k6_eq = -rho_*ux_*ux_*uy_+nu*(Fy-3.*Fy*ux_*ux_-6.*Fx*ux_*uy_);
	      k7_eq = -rho_*ux_*uy_*uy_+nu*(Fx-3.*Fx*uy_*uy_-6.*Fy*ux_*uy_);
	      k8_eq = rho_*cs2*cs2*(27.*ux_*ux_*uy_*uy_+1.)+12.*nu*ux_*uy_*(Fx*uy_+Fy*ux_);

	      /*Collision for 2nd order centered moments*/
	      k1_star = omega_vect1[1]*k1+omega_vect[1]*k1_eq;
	      k2_star = omega_vect1[2]*k2+omega_vect[2]*k2_eq;
	      k3_star = omega_vect1[3]*k3+omega_vect[3]*k3_eq;
	      k4_star = omega_vect1[4]*k4+omega_vect[4]*k4_eq;
	      k5_star = omega_vect1[5]*k5+omega_vect[5]*k5_eq;
	      k6_star = omega_vect1[6]*k6+omega_vect[6]*k6_eq;
	      k7_star = omega_vect1[7]*k7+omega_vect[7]*k7_eq;		 		 		 
	      k8_star = omega_vect1[8]*k8+omega_vect[8]*k8_eq;

	      collision_op[0] = k1_star*(-2*ux_+2*ux_*uy_2)+k2_star*(-2*uy_+2*ux_2*uy_)+k3_star*(-1+.5*ux_2+.5*uy_2)+k4_star*(-ux_2+uy_2)+4*ux_*uy_*k5_star+2*uy_*k6_star+2*ux_*k7_star+k8_star+rho_*(1-ux_2-uy_2+ux_2*uy_2);
	      collision_op[1] = k1_star*(0.5+ux_-0.5*uy_2-ux_*uy_2)+k2_star*(-ux_*uy_-ux_2*uy_)+0.25*k3_star*(1-ux_-ux_2-uy_2)+0.25*k4_star*(1+ux_+ux_2-uy_2)+k5_star*(-uy_-2*ux_*uy_)-uy_*k6_star+k7_star*(-.5-ux_)-.5*k8_star+0.5*rho_*(ux_+ux_2-ux_*uy_2-ux_2*uy_2);
	      collision_op[2] = k1_star*(-ux_*uy_-ux_*uy_2)+k2_star*(0.5+uy_-0.5*ux_2-ux_2*uy_)+0.25*k3_star*(1-uy_-ux_2-uy_2)+0.25*k4_star*(-1-uy_+ux_2-uy_2)+k5_star*(-ux_-2*ux_*uy_)+k6_star*(-.5-uy_)-ux_*k7_star-.5*k8_star+0.5*rho_*(uy_+uy_2-ux_2*uy_-ux_2*uy_2);
	      collision_op[3] = k1_star*(-0.5+ux_+0.5*uy_2-ux_*uy_2)+k2_star*(ux_*uy_-ux_2*uy_)+0.25*k3_star*(1+ux_-ux_2-uy_2)+0.25*k4_star*(1-ux_+ux_2-uy_2)+k5_star*(uy_-2*ux_*uy_)-uy_*k6_star+k7_star*(.5-ux_)-.5*k8_star+0.5*rho_*(-ux_+ux_2+ux_*uy_2-ux_2*uy_2);
	      collision_op[4] = k1_star*(ux_*uy_-ux_*uy_2)+k2_star*(-0.5+uy_+0.5*ux_2-ux_2*uy_)+0.25*k3_star*(1+uy_-ux_2-uy_2)+0.25*k4_star*(-1+uy_+ux_2-uy_2)+k5_star*(ux_-2*ux_*uy_)+k6_star*(.5-uy_)-ux_*k7_star-.5*k8_star+0.5*rho_*(-uy_+uy_2+ux_2*uy_-ux_2*uy_2);
	      collision_op[5] = 0.25*k1_star*(uy_+uy_2+2*ux_*uy_+2*ux_*uy_2)+0.25*k2_star*(ux_+ux_2+2*ux_*uy_+ux_2*uy_)+0.125*k3_star*(ux_+uy_+ux_2+uy_2)+0.125*k4_star*(-ux_+uy_-ux_2+uy_2)+k5_star*(0.25+.5*ux_+.5*uy_+ux_*uy_)+k6_star*(.25+.5*uy_)+k7_star*(.25+.5*ux_)+.25*k8_star+.25*rho_*(ux_*uy_+ux_2*uy_+ux_*uy_2+ux_2*uy_2);
	      collision_op[6] = 0.25*k1_star*(-uy_-uy_2+2*ux_*uy_+2*ux_*uy_2)+0.25*k2_star*(-ux_+ux_2-2*ux_*uy_+ux_2*uy_)+0.125*k3_star*(-ux_+uy_+ux_2+uy_2)+0.125*k4_star*(ux_+uy_-ux_2+uy_2)+k5_star*(-0.25+.5*ux_-.5*uy_+ux_*uy_)+k6_star*(.25+.5*uy_)+k7_star*(-.25+.5*ux_)+.25*k8_star+.25*rho_*(-ux_*uy_+ux_2*uy_-ux_*uy_2+ux_2*uy_2);
	      collision_op[7] = 0.25*k1_star*(uy_-uy_2-2*ux_*uy_+2*ux_*uy_2)+0.25*k2_star*(ux_-ux_2-2*ux_*uy_+ux_2*uy_)+0.125*k3_star*(-ux_-uy_+ux_2+uy_2)+0.125*k4_star*(ux_-uy_-ux_2+uy_2)+k5_star*(0.25-.5*ux_-.5*uy_+ux_*uy_)+k6_star*(-.25+.5*uy_)+k7_star*(-.25+.5*ux_)+.25*k8_star+.25*rho_*(ux_*uy_-ux_2*uy_-ux_*uy_2+ux_2*uy_2);
	      collision_op[8] = 0.25*k1_star*(-uy_+uy_2-2*ux_*uy_+2*ux_*uy_2)+0.25*k2_star*(-ux_-ux_2+2*ux_*uy_+ux_2*uy_)+0.125*k3_star*(ux_-uy_+ux_2+uy_2)+0.125*k4_star*(-ux_-uy_-ux_2+uy_2)+k5_star*(-0.25-.5*ux_+.5*uy_+ux_*uy_)+k6_star*(-.25+.5*uy_)+k7_star*(.25+.5*ux_)+.25*k8_star+.25*rho_*(-ux_*uy_-ux_2*uy_+ux_*uy_2+ux_2*uy_2);
				 
	      for(int k=0;k<9;k++)
		{
		  nx = (x + c[k][0]+m_Dx)%m_Dx;
		  ny = (y + c[k][1]+m_Dy)%m_Dy;
		  fin[IDX(x,y,k)] = collision_op[k];
		  fout[IDX(nx,ny,k)] = collision_op[k];
		}
	    }
	}
    }
    
}
