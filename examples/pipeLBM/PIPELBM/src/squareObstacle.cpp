#include "Obstacle.h"

squareObstacle::squareObstacle(int x0, int y0, int R) : Obstacle(R), m_x0(x0), m_y0(y0)
{
  m_xmax = m_x0 + m_R;
  m_ymax = m_y0 + m_R;
}
squareObstacle::~squareObstacle()
{
}
void squareObstacle::markSolidNodes(bool** isFluid)
{
  for(int x=m_x0+1;x<m_xmax;x++)
    {
      for(int y=m_y0+1;y<m_ymax;y++)
	{
	  isFluid[x][y] = false;
	}
    }
}
void squareObstacle::applyBoundary(double *fin, double *fout, int Dy)
{
    for(int x=m_x0+1;x<m_xmax;x++)
    {
      fout[IDX_OBS(x,m_y0,7)] = fin[IDX_OBS(x,m_y0,5)];
      fout[IDX_OBS(x,m_y0,4)] = fin[IDX_OBS(x,m_y0,2)];
      fout[IDX_OBS(x,m_y0,8)] = fin[IDX_OBS(x,m_y0,6)];

      fout[IDX_OBS(x,m_ymax,5)] = fin[IDX_OBS(x,m_ymax,7)];
      fout[IDX_OBS(x,m_ymax,2)] = fin[IDX_OBS(x,m_ymax,4)];
      fout[IDX_OBS(x,m_ymax,6)] = fin[IDX_OBS(x,m_ymax,8)];
    }
  for(int y=m_y0+1;y<m_ymax;y++)
    {
      fout[IDX_OBS(m_x0,y,6)] = fin[IDX_OBS(m_x0,y,8)];
      fout[IDX_OBS(m_x0,y,3)] = fin[IDX_OBS(m_x0,y,1)];
      fout[IDX_OBS(m_x0,y,7)] = fin[IDX_OBS(m_x0,y,5)];

      fout[IDX_OBS(m_xmax,y,8)] = fin[IDX_OBS(m_xmax,y,6)];
      fout[IDX_OBS(m_xmax,y,1)] = fin[IDX_OBS(m_xmax,y,3)];
      fout[IDX_OBS(m_xmax,y,5)] = fin[IDX_OBS(m_xmax,y,7)];
    }
  fout[IDX_OBS(m_x0,m_y0,7)] = fin[IDX_OBS(m_x0,m_y0,5)];
  fout[IDX_OBS(m_x0,m_ymax,6)] = fin[IDX_OBS(m_x0,m_ymax,8)];
  fout[IDX_OBS(m_xmax,m_ymax,5)] = fin[IDX_OBS(m_xmax,m_ymax,7)];
  fout[IDX_OBS(m_xmax,m_y0,8)] = fin[IDX_OBS(m_xmax,m_y0,6)];
}
double squareObstacle::getViscousTop()
{
  return m_viscousTop;
}
double squareObstacle::getViscousBot()
{
  return m_viscousBot;
}
double squareObstacle::getViscousFront()
{
  return m_viscousFront;
}
double squareObstacle::getViscousRear()
{
  return m_viscousRear;
}
double squareObstacle::getpFront()
{
  return m_pFront;
}
double squareObstacle::getpRear()
{
  return m_pRear;
}
