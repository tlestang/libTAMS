#include "Obstacle.h"

void squareObstacle::computeStress(double *fin, int Dy, double tau)
{
  int c[9][2] = {{0,0}, {1,0}, {0,1}, {-1,0}, {0,-1}, {1,1}, {-1,1}, {-1,-1}, {1,-1}};
  double w[9]={4.0/9.0, 1.0/9.0, 1.0/9.0, 1.0/9.0, 1.0/9.0, 1.0/36.0, 1.0/36.0, 1.0/36.0, 1.0/36.0};
  
  double ot = 1./3.;
  double rho0 = 1.0;
  int x0, y0;
  double eu, eueu, u2;
  double rho_, ux, uy, Pi_xx, Pi_xy;
  double fneq, ftemp, feq;
  double totalForce;
  double coeff_force = 1.0-.5*(1./tau);

  /*West side*/
  /*Compute Pi_xx*/
  x0 = m_x0;
  m_viscousFront = 0.0;
  m_pFront = 0.0;
  for(int y=m_y0;y<m_ymax+1;y++)
    {
      /*Compute local macro. fields*/
      rho_ = 0.0; ux = 0.0; uy = 0.0;
      for(int k=0;k<9;k++)
	{
	  ftemp = fin[IDX_OBS(x0,y,k)];
	  rho_ += ftemp;
	  ux += ftemp*c[k][0];
	  uy += ftemp*c[k][1];
	}
      uy /= rho_;
      ux /= rho_;
      /*Compute tensor Pi1*/
      Pi_xx = 0.0;
      u2 = -1.5*(ux*ux + uy*uy);
      for(int k=0;k<9;k++)
	{
	  eu = c[k][0]*ux + c[k][1]*uy;
	  eueu = 4.5*eu*eu;
	  feq = w[k]*rho_*(1.0+3.0*eu+eueu+u2);
	  fneq = fin[IDX_OBS(x0,y,k)] - feq;
	  Pi_xx += fneq*c[k][0]*c[k][0];
	}
      /*Compute force*/
      m_viscousFront += coeff_force*Pi_xx;
      m_pFront += rho_*ot;
    }
  /*East side*/
  x0 = m_xmax;
  m_viscousRear = 0.0;
  m_pRear = 0.0;
  for(int y=m_y0;y<m_ymax+1;y++)
    {
      /*Compute local macro. fields*/
      rho_ = 0.0; ux = 0.0; uy = 0.0;
      for(int k=0;k<9;k++)
	{
	  ftemp = fin[IDX_OBS(x0,y,k)];
	  rho_ += ftemp;
	  ux += ftemp*c[k][0];
	  uy += ftemp*c[k][1];
	}
      uy /= rho_;
      ux /= rho_;
      /*Compute tensor Pi1*/
      Pi_xx = 0.0;
      u2 = -1.5*(ux*ux + uy*uy);
      for(int k=0;k<9;k++)
	{
	  eu = c[k][0]*ux + c[k][1]*uy;
	  eueu = 4.5*eu*eu;
	  feq = w[k]*rho_*(1.0+3.0*eu+eueu+u2);
	  fneq = fin[IDX_OBS(x0,y,k)] - feq;
	  Pi_xx += fneq*c[k][0]*c[k][0];
	}
      m_viscousRear += -coeff_force*Pi_xx;
      m_pRear += -rho_*ot;
    }
  /*North side*/
  y0 = m_ymax;
  m_viscousTop = 0.0;
  for(int x=m_x0;x<m_xmax+1;x++)
    {
      /*Compute local macro. fields*/
      rho_ = 0.0; ux = 0.0; uy = 0.0;
      for(int k=0;k<9;k++)
	{
	  ftemp = fin[IDX_OBS(x,y0,k)];
	  rho_ += ftemp;
	  ux += ftemp*c[k][0];
	  uy += ftemp*c[k][1];
	}
      uy /= rho_;
      ux /= rho_;
      /*Compute tensor Pi1*/
      Pi_xy = 0.0;
      u2 = -1.5*(ux*ux + uy*uy);
      for(int k=0;k<9;k++)
	{
	  eu = c[k][0]*ux + c[k][1]*uy;
	  eueu = 4.5*eu*eu;
	  feq = w[k]*rho_*(1.0+3.0*eu+eueu+u2);
	  fneq = fin[IDX_OBS(x,y0,k)] - feq;
	  Pi_xy += fneq*c[k][0]*c[k][1];
	}
      m_viscousTop += -coeff_force*Pi_xy;
    }
  /*South side*/
  y0 = m_y0;
  m_viscousBot = 0.0;
  for(int x=m_x0;x<m_xmax+1;x++)
    {
      rho_ = 0.0; ux = 0.0; uy = 0.0;
      /*Compute local macro. fields*/
      for(int k=0;k<9;k++)
	{
	  ftemp = fin[IDX_OBS(x,y0,k)];
	  rho_ += ftemp;
	  ux += ftemp*c[k][0];
	  uy += ftemp*c[k][1];
	}
      uy /= rho_;
      ux /= rho_;
      /*Compute tensor Pi1*/
      Pi_xy = 0.0;
      u2 = -1.5*(ux*ux + uy*uy);
      for(int k=0;k<9;k++)
	{
	  eu = c[k][0]*ux + c[k][1]*uy;
	  eueu = 4.5*eu*eu;
	  feq = w[k]*rho_*(1.0+3.0*eu+eueu+u2);
	  fneq = fin[IDX_OBS(x,y0,k)] - feq;
	  Pi_xy += fneq*c[k][0]*c[k][1];
	}
      m_viscousBot += coeff_force*Pi_xy;
    }
  m_drag = m_viscousBot+m_viscousTop+m_viscousRear+m_viscousFront+m_pRear+m_pFront;
}
