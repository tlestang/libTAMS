#include "pipeLBM.h"

using namespace std;

void pipeLBM::computeStressOnWalls()
{
  int c[9][2] = {{0,0}, {1,0}, {0,1}, {-1,0}, {0,-1}, {1,1}, {-1,1}, {-1,-1}, {1,-1}};
  double w[9]={4.0/9.0, 1.0/9.0, 1.0/9.0, 1.0/9.0, 1.0/9.0, 1.0/36.0, 1.0/36.0, 1.0/36.0, 1.0/36.0};
  
  double ot = 1./3.;
  double rho0 = 1.0;
  int x0, y0;
  double eu, eueu, u2;
  double rho_, ux, uy, Pi_xx, Pi_xy;
  double fneq, ftemp, feq;
  double coeff_force = 1.0-.5*(1./m_tau);
  /*North side*/
  y0 = m_Dy-1;
  m_viscousTop = 0.0;
  for(int x=0;x<m_Dx;x++)
    {
      /*Compute local macro. fields*/
      rho_ = 0.0; ux = 0.0; uy = 0.0;
      for(int k=0;k<9;k++)
	{
	  ftemp = fin[IDX(x,y0,k)];
	  rho_ += ftemp;
	  ux += ftemp*c[k][0];
	  uy += ftemp*c[k][1];
	}
      uy /= rho_;
      ux /= rho_;
      /*Compute tensor Pi1*/
      Pi_xy = 0.0;
      u2 = -1.5*(ux*ux + uy*uy);
      for(int k=0;k<9;k++)
	{
	  eu = c[k][0]*ux + c[k][1]*uy;
	  eueu = 4.5*eu*eu;
	  feq = w[k]*rho_*(1.0+3.0*eu+eueu+u2);
	  fneq = fin[IDX(x,y0,k)] - feq;
	  Pi_xy += fneq*c[k][0]*c[k][1];
	}
      m_viscousTop += coeff_force*Pi_xy;
    }

    /*South side*/
  y0 = 0;
  m_viscousBot = 0.0;
  for(int x=0;x<m_Dx;x++)
    {
      /*Compute local macro. fields*/
      rho_ = 0.0; ux = 0.0; uy = 0.0;
      for(int k=0;k<9;k++)
	{
	  ftemp = fin[IDX(x,y0,k)];
	  rho_ += ftemp;
	  ux += ftemp*c[k][0];
	  uy += ftemp*c[k][1];
	}
      uy /= rho_;
      ux /= rho_;
      /*Compute tensor Pi1*/
      Pi_xy = 0.0;
      u2 = -1.5*(ux*ux + uy*uy);
      for(int k=0;k<9;k++)
	{
	  eu = c[k][0]*ux + c[k][1]*uy;
	  eueu = 4.5*eu*eu;
	  feq = w[k]*rho_*(1.0+3.0*eu+eueu+u2);
	  fneq = fin[IDX(x,y0,k)] - feq;
	  Pi_xy += fneq*c[k][0]*c[k][1];
	}
      m_viscousBot += -coeff_force*Pi_xy;
    }
}
