#include "pipeLBM.h"

void pipeLBM::applyInletPoiseuille()
{
  int c[9][2] = {{0,0}, {1,0}, {0,1}, {-1,0}, {0,-1}, {1,1}, {-1,1}, {-1,-1}, {1,-1}};
  double w[9]={4.0/9.0, 1.0/9.0, 1.0/9.0, 1.0/9.0, 1.0/9.0, 1.0/36.0, 1.0/36.0, 1.0/36.0, 1.0/36.0};
  int x0 = 0;
  double u2; double eu, eueu; double rho0=1.0;
  double uu0;
  for(int y=0;y<m_Dy;y++)
    {
      uu0 = - m_U*(4*(y+0.5)*(y+0.5-m_Dy))/(m_Dy*m_Dy);
      u2 = uu0*uu0;
      for(int k=0;k<9;k++)
	{
	  eu = c[k][0]*uu0;
	  eueu = 4.5*eu*eu;
	  fout[IDX(x0,y,k)] = w[k]*rho0*(1.0+3.0*eu+eueu-1.5*u2+4.5*(eu*eu*eu-eu*u2));
	}
    }
}

  
