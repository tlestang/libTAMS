#include <cstdlib>
#include <fstream>
#include <sstream>
#include <cmath>
#include <cstring>
#include "pipeLBM.h"

bool pipeLBM::initFromFile(std::string fullPath)
{
  bool errorFlag = false;
  int N = m_Dx*m_Dy*9;
  std::ifstream stateFile;
  stateFile.open(fullPath.c_str(), std::ios::binary);
  if(stateFile.is_open())
    {
      stateFile.read((char*)&fin[0], N*sizeof(double));
    }
  else
    {
      errorFlag = true;
    }
  stateFile.close();

  initMacro();
  
  return errorFlag;
}

void pipeLBM::initFromState(double* ptToState)
{
  int N = m_Dx*m_Dy*9;
  memcpy(fin, ptToState, N*sizeof(double));
  
  initMacro();
}

bool pipeLBM::initFromRandom(std::string path, std::string root, int NN, int NbFiles)
{
  bool errorFlag = false;
  
  double *f_prim;
  int N = m_Dx*m_Dy*9;

  f_prim = new double[N];
  for(int i=0;i<N;i++)
    {
      fin[i] = 0.0;
    }

  double random_prefactor;
  double sum=0.0;
  int popFileIdx;
  std::ifstream stateFile;
  std::stringstream dummyss;
  std::string fullPath;
  for (int nn=0;nn<NN;nn++)
    {
      random_prefactor = rand() / (double) RAND_MAX;
      sum += random_prefactor;

      popFileIdx = rand() % NbFiles;
      dummyss << "_" << popFileIdx << ".datout";
      fullPath= path + root + dummyss.str();
      dummyss.str(std::string());
      dummyss.clear();
      
      stateFile.open(fullPath.c_str(), std::ios::binary);
      if(stateFile.is_open())
	{
	  stateFile.read((char*)&f_prim[0], N*sizeof(double));
	  for(int i=0;i<N;i++)
	    {
	      fin[i] = fin[i] + random_prefactor*f_prim[i];
	    }
	}
      else
	{
	  errorFlag = true;
	}
      stateFile.close();
    }
  
  delete[] f_prim;
  for(int i =0 ; i<N ; i++)
    {
      fin[i] = fin[i]/sum;
    }

  initMacro();

  return errorFlag;
}
  
bool pipeLBM::makePerturbation(std::string path, std::string root, int NN, int NbFiles, double eps)
{
  bool errorFlag = false;
  
  double *f_prim, *fPerturb;
  int N = m_Dx*m_Dy*9;

  f_prim = new double[N];
  fPerturb = new double[N];
  for(int i=0;i<N;i++)
    {
      f_prim[i] = 0.0;
    }

  double random_prefactor,oneOvA,a,weight,norm_0,norm_prim,sum_prefactors;
  double sum=0.0;
  int popFileIdx;
  std::ifstream stateFile;
  std::stringstream dummyss;
  std::string fullPath;
  for (int nn=0;nn<NN;nn++)
    {
      random_prefactor = rand() / (double) RAND_MAX;
      sum += random_prefactor;

      popFileIdx = rand() % NbFiles;
      dummyss << "_" << popFileIdx << ".datout";
      fullPath= path + root + dummyss.str();
      dummyss.str(std::string());
      dummyss.clear();
      
      stateFile.open(fullPath.c_str(), std::ios::binary);
      if(stateFile.is_open())
	{
	  stateFile.read((char*)&fPerturb[0], N*sizeof(double));
	  for(int i=0;i<N;i++)
	    {
	      f_prim[i] += random_prefactor*fPerturb[i];
	    }
	  stateFile.close();
	}
      else
	{
	  errorFlag = true;
	}
    }
  norm_0 = 0.0; norm_prim = 0.0;
  for(int i=0;i<N;i++)
    {
      norm_0 += fin[i]*fin[i];
      norm_prim += f_prim[i]*f_prim[i];
    }
  norm_0 = sqrt(norm_0); norm_prim = sqrt(norm_prim);
  weight = norm_0 / norm_prim;
  a = 1.0 + sum_prefactors*eps*weight;
  oneOvA = 1./a;
  for(int i=0;i<N;i++)
    {
      fin[i] += eps*f_prim[i]*weight;
      fin[i] = fin[i]*oneOvA;
    }

  initMacro();
  
  delete[] f_prim;
  delete[] fPerturb;

  return errorFlag;
}
void pipeLBM::initMacro()
{
  int c[9][2] = {{0,0}, {1,0}, {0,1}, {-1,0}, {0,-1}, {1,1}, {-1,1}, {-1,-1}, {1,-1}};
  double w[9]={4.0/9.0, 1.0/9.0, 1.0/9.0, 1.0/9.0, 1.0/9.0, 1.0/36.0, 1.0/36.0, 1.0/36.0, 1.0/36.0};
  
  double ux_ = 0.0;
  double uy_ = 0.0;
  double rho_ = 0.0;
  
  for(int x=0;x<m_Dx;x++)
    {
      for(int y=0;y<m_Dy;y++)
	{
	  if(isFluid[x][y])
	    {
	      ux_ = 0.0;
	      uy_ = 0.0;
	      rho_ = 0.0;
	      for(int k=0;k<9;k++)
		{
		  ux_ += c[k][0]*fin[IDX(x,y,k)];
		  uy_ += c[k][1]*fin[IDX(x,y,k)];
		  rho_ += fin[IDX(x,y,k)];
		}
	      ux[idx(x,y)] = (ux_+0.5*m_F0)/rho_;
	      uy[idx(x,y)] = uy_/rho_;
	      rho[idx(x,y)] = rho_;
	    }
	  else
	    {
	      ux[idx(x,y)] = 0.0;
	      uy[idx(x,y)] = 0.0;
	      rho[idx(x,y)] = 1.0;
	    }
	}
    }
}
