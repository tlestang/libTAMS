#include "pipeLBM.h"

void pipeLBM::applyOutletOpen()
{
  int c[9][2] = {{0,0}, {1,0}, {0,1}, {-1,0}, {0,-1}, {1,1}, {-1,1}, {-1,-1}, {1,-1}};
  double w[9]={4.0/9.0, 1.0/9.0, 1.0/9.0, 1.0/9.0, 1.0/9.0, 1.0/36.0, 1.0/36.0, 1.0/36.0, 1.0/36.0};
  
  int x0 = m_Dx-1;
  double u2; double eu, eueu; double rho0=1.0;
  double feq, fneq, Sxx, Sxy, Syy, dxx, dxy, dyx;
  double cs2 = 1./3.;
  double ux_[m_Dy]; double uy_[m_Dy]; double rho_[m_Dy];

  /*Extrapolate velocity and density along x-axis*/
  /*Linear interp.*/
  for(int y=1;y<m_Dy-1;y++)
    {
      ux_[y] = ux[idx(m_Dx-3,y)] + 2.*(ux[idx(m_Dx-2,y)]-ux[idx(m_Dx-3,y)]);
      uy_[y] = uy[idx(m_Dx-3,y)] + 2.*(uy[idx(m_Dx-2,y)]-uy[idx(m_Dx-3,y)]);
      // ux_[y] = 0.05;
      // uy_[y] = 0.05;
      //rho_[y] = rho[idx(m_Dx-3,y)] + 2.*(rho[idx(m_Dx-2,y)]-rho[idx(m_Dx-3,y)]);
      rho_[y] = 1.;
    }
  ux_[0] = ux[idx(x0,0)]; uy_[0]=uy[idx(x0,0)];
  ux_[m_Dy-1] = ux[idx(x0,m_Dy-1)]; uy_[m_Dy-1]=uy[idx(x0,m_Dy-1)];
  // uy_[0] = 0; uy_[m_Dy-1] = 0;
  // ux_[0] = 0; ux_[m_Dy-1] = 0;
  
  for(int y=1;y<m_Dy-1;y++)
    {
      u2 = -1.5*(ux_[y]*ux_[y]+uy_[y]*uy_[y]);
      /*Compute strain rate S on the node*/
      dxx = 0.5*(3.*ux_[y] - 4.*ux[idx(x0-1,y)]+ux[idx(x0-2,y)]);
      Sxx = dxx;
      dyx = 0.5*(3.*uy_[y] - 4.*uy[idx(x0-1,y)] + uy[idx(x0-2,y)]);
      dxy = 0.5*(ux_[y+1] - ux_[y-1]);
      Sxy = 0.5*(dxy + dyx);
      Syy = 0.5*(uy_[y+1] - uy_[y-1]);

      for(int k=0;k<9;k++)
	{
	  eu = c[k][0]*ux_[y]+c[k][1]*uy_[y];
	  eueu = 4.5*eu*eu;
	  
	  feq = w[k]*rho_[y]*(1.0+3.0*eu+eueu+u2);
	  fneq = (c[k][0]*c[k][0]-cs2)*Sxx + 2.*c[k][0]*c[k][1]*Sxy + (c[k][1]*c[k][1]-cs2)*Syy;
	  fneq *= - (rho_[y]*w[k]*m_tau/cs2);

	  fout[IDX(x0,y,k)] = feq + fneq;
	}
    }
  /*South East Corner*/
  fout[IDX(m_Dx-1,0,3)] = fin[IDX(m_Dx-1,0,1)];
  fout[IDX(m_Dx-1,0,7)] = fin[IDX(m_Dx-1,0,5)];
  /*North East Corner*/
  fout[IDX(m_Dx-1,m_Dy-1,3)] = fin[IDX(m_Dx-1,m_Dy-1,1)];
  fout[IDX(m_Dx-1,m_Dy-1,6)] = fin[IDX(m_Dx-1,m_Dy-1,8)];
}
