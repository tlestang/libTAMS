#include <fstream>
#include <sstream>
#include "pipeLBM.h"

using namespace std;

void pipeLBM::writeVTK(int time)
{
  /// Create filename
  stringstream fileName;
  fileName << "fluid_t" << time << ".vtk";
  string output_filename = fileName.str();
  ofstream output_file;
  double rho0 = 1.0;

  /// Open file
  output_file.open(output_filename.c_str());

  /// Write VTK header
  output_file << "# vtk DataFile Version 3.0\n";
  output_file << "fluid_state\n";
  output_file << "ASCII\n";
  output_file << "DATASET RECTILINEAR_GRID\n";
  output_file << "DIMENSIONS " << m_Dx << " " << m_Dy << " 1" << "\n";
  output_file << "X_COORDINATES " << m_Dx << " float\n";
  for(int i = 0; i < m_Dx; ++i)
    output_file << i << " ";
  output_file << "\n";
  output_file << "Y_COORDINATES " << m_Dy  << " float\n";
  for(int j = 0; j < m_Dy ; ++j)
    output_file << j  << " ";
  output_file << "\n";
  output_file << "Z_COORDINATES " << 1 << " float\n";
  output_file << 0 << "\n";
  output_file << "POINT_DATA " << (m_Dx) * (m_Dy) << "\n";

  /// Write density difference
  output_file << "SCALARS density float 1\n";
  output_file << "LOOKUP_TABLE default\n";
  for(int Y =0; Y < m_Dy ; ++Y)
    for(int X = 0; X < m_Dx; ++X)
      output_file <<  rho[idx(X,Y)] << "\n";

  /// Write velocity
  output_file << "VECTORS velocity_vector float\n";
  for(int Y = 0; Y < m_Dy ; ++Y)
    for(int X = 0; X < m_Dx; ++X)
      output_file << ux[idx(X,Y)] << " " << uy[idx(X,Y)] << " 0\n";

  /// Close file
  output_file.close();
}
