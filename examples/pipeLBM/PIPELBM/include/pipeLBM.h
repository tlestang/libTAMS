#ifndef _DEF_PIPELBM
#define _DEF_PIPELBM

#define IDX(i,j,k) ((k) + 9*( (j)+ m_Dy*(i) ) )
#define idx(i,j) ((j) + m_Dy * (i) )

#include <string>
#include "Obstacle.h"

class pipeLBM{
 public:
  pipeLBM(int Dx, int Dy);
  ~pipeLBM();
  void setParameters(double tau, double U, double F0);
  void setSpongeLayer(int spgeStart, int spgeEnd);
  void setObstacles(Obstacle *obs);
  void setObstacles(Obstacle *obs[], int nbObs);
  void drawGeometry(const char* fileName);
  bool setInletBC(std::string methodID);
  bool setOutletBC(std::string methodID);
  void initializeToEquilibrium();
  void initMacro();
  bool initFromFile(std::string fullPath);
  void initFromState(double* ptToState);
  bool initFromRandom(std::string path, std::string root, int NN, int NbFiles);
  bool makePerturbation(std::string path, std::string root, int NN, int NbFiles, double eps);
  void applyInletPoiseuille();
  void applyOutletOpen();
  void doStreamingAndCollision();
  void doStreamingAndCollision_BGK();
  void computeStress(Obstacle *obs);
  double* getState();
  void writeVTK(int time);
  void computeFieldsInBulk();
  void computeFieldsAlongBorders();
  void computeStressOnWalls();
  void advanceOneTimestep();
  void advanceOneTimestep(Obstacle *obs);
  void advanceOneTimestep(Obstacle *obs[], int nbObs);
  void advanceOneTimestep_parallel(int nbProcs);
  void advanceOneTimestep_parallel(Obstacle *obs, int nbProcs);
  void advanceOneTimestep_parallel(Obstacle *obs[], int nbObs, int nbProcs);
  void advanceOneTimestep_BGK();
  void advanceOneTimestep_BGK(Obstacle *obs);
  void advanceOneTimestep_BGK(Obstacle *obs[], int nbObs);
  void applyWallsBounceBack();
  void applyInletPeriodic();
  void applyOutletPeriodic();
  void displayPercentage(int currentIteration, int totalNbOfIterations);
  double getLongVelocityAtPoint(int x, int y);
  double getTransVelocityAtPoint(int x, int y);
  double getDensityAtPoint(int x, int y);
  double getVorticityAtPoint(int x, int y);
  double getForceOnWalls();

  void doStreamingAndCollision_parallel(int nbProcs);
  /*Ponters to member functions*/
  void (pipeLBM::*applyInletBC)();
  void (pipeLBM::*applyOutletBC)();

  /*Special methods for tests*/
  void test_applyBoundary(Obstacle *obs);
 private:
  double *fin, *fout; /*Pointers to the populations arrays*/
  double *ux, *uy, *rho; /*Pointers to the arrays for macro. fields*/
  bool **isFluid; /*Pointer to type of node (fluid or solid)*/

  int m_Dx, m_Dy;
  double m_tau, m_U, m_F0;
  int m_spgeStart, m_spgeEnd;
  int m_percent;
  double m_viscousTop;
  double m_viscousBot;

};

#endif
