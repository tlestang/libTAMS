#include <iostream>
#include <cmath>
#include "TAMSAVG.h"
#include "makeSelection.h"
#include "drawParent.h"
#include "copyTrajToBranchingPoint.h"
#include "./dynamics/OUdynamics.h"

using namespace std;

int main()
{
  srand(1);
  
  int Nc = 4; // Number of initial traj.
  double dt = 0.001; // Discretization timestep
  double T_a = 20; // Duration of the traj.
  double Tavg = 5; // Number of timesteps
  int Nt = floor(T_a/dt);
  int NtAVG = floor(Tavg/dt);

  // Construction of objects for TAMS and underlying dynamics
  TAMSAVG<double>* myTAMS = new TAMSAVG<double>(Nc, Nt, NtAVG);
  OUdynamics* myOU = new OUdynamics(dt);

  double x;
  string dirName = "./";
  int Niter = 1; // Number of TAMS iterations
  int nrep = 1; // Number of times the whole TAMS will be repeated.

  /* Containers for the maximum over resampled (or pruned) trajectories
     and statistical weight of the corresponding trajectory */
  vector<double> amax;
  vector<double> weights;

  int K, tBranch;
  double Z;
  double w = 1;
  double unSurN = 1./Nc;
  
  // loop over repetitions (nrep)
  for (int iCount=0;iCount<nrep;iCount++)
    {
      amax.clear();
      weights.clear();
      // TAMS init. step
      for(int j=0;j<Nc;j++)
	{
	  myOU->init();
	  myTAMS->setActiveTraj(j);
	  for(int t=0;t<Nt;t++)
	    {
	      myOU->advanceOneTimestep();
	      x = myOU->getState();
	      myTAMS->setCostFunction(x, t);
	      myTAMS->setState(x, t);
	    }
	}
      /* The computeMaxima() method computes the maximum over each trajectory
	 and updates the value of the threshold Z */
      myTAMS->computeMaxima();
      
#ifdef _TRAJ_IO
      // writeCostFunction(string dirName, int iter) writes the cost function
      // corresponding to the Nc current trajectories on disk.
      // dirName: Name of the directory the trajectories will be written in.
      // iter: Index of the iteration corresponding to the traj.
      // 	0 is initialization.
      myTAMS->writeTrajectories(dirName, 0);
      myTAMS->writeCostFunction(dirName, 0);
#endif
      
      // Loop over iterations of the TAMS algorithm
      for (int i=0;i<Niter;i++)
	{
	  /* Selects the K trajectories which maximum corresponds 
             to the threshold Z */
	  K = myTAMS->makeSelection();
	  /* Now for each one of these traj., do: */
	  for (int k=0; k<K;k++)
	    {
	      // Picks a parent traj among the (Nc-K) remaining traj.
	      myTAMS->drawParent();
	      myTAMS->setActiveTraj(k); // Mark the trajectory as ACTIVE
	      
	      /* In this case we record the max and weight of the
		 __selected__ traj.*/
	      amax.push_back(myTAMS->getMax());
	      weights.push_back(w);

	      /* Parent traj. overwrites the selected traj.
                 until branching point*/
	      tBranch = myTAMS->copyTrajToBranchingPoint();
	      x = myTAMS->getRestart();
	      myOU->init(x);
	      /* Compute dynamics from tBranch to T_a */
	      for (int t=tBranch;t<Nt;t++)
		{
		  myOU->advanceOneTimestep();
		  x = myOU->getState();
		  myTAMS->setCostFunction(x, t); // Updates cost function
		  // In this example trajectories are kept in memory.
		  myTAMS->setState(x, t);
		}
	      /*Go through the whole current active trajectory 
                and compute maximum. If needed, the threshold Z
                is updated */
	      myTAMS->updateMaxima();
	    }
	  /* Compute the statistical weight corresponding to 
	     the K selected traj. */
	  w = w*(1.-K*unSurN);
	  
#ifdef _TRAJ_IO
	  myTAMS->writeCostFunction(dirName, i+1);
	  myTAMS->writeTrajectories(dirName, i+1);
#endif
	}
      /* End of iterations.
         Records the max and weight for the  final Nc traj.
      */
      
      for(int j=0;j<Nc;j++)
	{
	  amax.push_back(myTAMS->getMax(j));
	  weights.push_back(w);
	}
      /*
	Writes arrays to disk
      */
      ofstream amaxfile("amax.dat", ios::binary | ios::app);
      amaxfile.write((char*)&amax[0], amax.size()*sizeof(double));
      amaxfile.close();
      ofstream wfile("weights.dat", ios::binary | ios::app);
      wfile.write((char*)&weights[0], weights.size()*sizeof(double));
      wfile.close();
    }
      
  delete myTAMS;
  delete myOU;
}
  
