clf;
close all;
clear all;
figure(1)
hold on

cVec = {'b', 'g', 'k', 'r'}

prefix = '../' 
for i=1:4
  fid = fopen([prefix 'traj_' num2str(i-1) '_costFunc_iter_0.dat'], 'r');
  f = fread(fid, [1 Inf], 'double');
  fclose(fid);
  fid = fopen([prefix 'traj_' num2str(i-1) '_costFuncInt_iter_0.dat'], 'r');
  F = fread(fid, [1 Inf], 'double');
  fclose(fid);

  ndtAVG = length(f) - length(F) + 1;
  plot(f, 'color', cVec{i});
  plot([ndtAVG:length(f)], F/ndtAVG, 'color', cVec{i}, 'linewidth', 2);
end
%legend({'Traj 0', 'Traj 1', 'Traj 2', 'Traj 3'})
