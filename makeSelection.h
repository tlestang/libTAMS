#ifndef _MAKE_SELECTION
#define _MAKE_SELECTION
template<typename T>
int TAMS<T>::makeSelection()
{
  int jj, K=0;
  double a;
  computeZ();
  for(int j=0;j<m_Nc;j++)
    {
      jj = m_idx[j];
      if(m_amax[jj]==m_Z)
	{
	  a = m_idx[K];
	  m_idx[K] = m_idx[j];
	  m_idx[j] = a;
	  K++;
	}
    }
  m_K = K;
  return m_K;
}
#endif
