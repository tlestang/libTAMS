#ifndef _LIB_TAMS
#define _LIB_TAMS

#include "TAMS.h"
#include "TAMSAVG.h"
#include "rejecTAMS.h"
#include "makeSelection.h"
#include "drawParent.h"
#include "copyTrajToBranchingPoint.h"

#endif
