template<typename T>
int TAMS<T>::copyTrajToBranchingPoint()
{
  double Cf;
  int t=0;
  if(m_stateArray != NULL)
    {
      do
	{
	  Cf = m_costFunction[m_parent][t];
	  m_costFunction[m_jj][t]=Cf;
	  m_stateArray[m_jj][t] = m_stateArray[m_parent][t];
	  t++;
	}
      while(Cf<=m_Z);
    }
  else
    {
      do
	{
	  Cf = m_costFunction[m_parent][t];
	  m_costFunction[m_jj][t]=Cf;
	  t++;
	}
      while(Cf<=m_Z);
    }
  /*Update value of tBranch
    it is the *first* time at which m_Z was (strictly) passed*/
  m_tBranch = t-1;
  /*Update maximum for partial traj. jj
    By construction is is the last step*/
  m_amax[m_jj] = Cf;

  return m_tBranch;
}

template<typename T>
int TAMSAVG<T>::copyTrajToBranchingPoint()
{
  double Cf, intCf;
  double Z = this->m_Z;
  int jj = this->m_jj;
  int parent = this->m_parent;
  int t=0;
  for(int tt=0;tt<m_NtAVG-1;tt++)
    {
      Cf = (this->m_costFunction)[parent][tt];
      (this->m_costFunction)[jj][tt]=Cf;
    }
  do
    {
      Cf = (this->m_costFunction)[parent][t+m_NtAVG-1];
      (this->m_costFunction)[jj][t+m_NtAVG-1]=Cf;
      
      intCf = m_costFunctionInt[parent][t];
      m_costFunctionInt[jj][t]=intCf;
      t++;
    }
  while(intCf<=Z);
  if((this->m_stateArray) != NULL)
    {
      for(int tt=0;tt<t-1+m_NtAVG;tt++)
	{
	  (this->m_stateArray)[jj][tt] = (this->m_stateArray)[parent][tt];
	}
    }
  /*Update value of tBranch
    it is the *first* time at which m_Z was (strictly) passed*/
  (this->m_tBranch) = t-1+m_NtAVG-1;
  /*Update maximum for partial traj. jj
    By construction is is the last step*/
  (this->m_amax)[jj] = intCf;

  return (this->m_tBranch);
}
/* template<typename T> */
/* int rejecTAMS<T>::copyTrajToBranchingPoint() */
/* { */
/*   double Cf; //Buffer for current value ofthe cost func. */
/*   double Ztilda = this->m_Z - m_dZ; //Shifted threshold */
/*                                     //until which the parent traj is copied. */
/*   int t=0; //Time counter */
/*   int parent = this->m_parent; //Index of the parent traj */
  
/*   if((this->m_stateArray) != NULL) //If m_stateArray was allocated */
/*                                    //(low dimensional systems) */
/*     { */
/*       do //do-while because at the very least the first step is copied. */
/* 	{ */
/* 	  Cf = (this->m_costFunction)[parent][t]; */
/* 	  m_trialCostFunction[t]=Cf; //Copy parent */
/* 	  m_trialStateArray[t] = (this->m_stateArray)[parent][t];//Copy parent */
/* 	  t++; */
/* 	} */
/*       while(!(Cf>Ztilda));//Until the parent's cost. func. is *strictly* above Ztilda. */
/*     } */
/*   else  //If m_stateArray was allocated */
/*         //(high dimensional systems) */
/*     { */
/*       do */
/* 	{ */
/* 	  Cf = (this->m_costFunction)[parent][t]; */
/* 	  m_trialCostFunction[t]=Cf; */
/* 	  t++; */
/* 	} */
/*       while(Cf<=Ztilda); */
/*     } */
/*   /\*Update value of tBranch */
/*     it is the *first* time at which m_Ztilda was (strictly) passed*\/ */
/*   (this->m_tBranch) = t-1; */

/*   return (this->m_tBranch); */
/* } */
